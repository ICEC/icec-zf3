<?php

namespace Application;

use Application\Controller\Authentication\AuthenticationAjaxController;
use Application\Controller\UserAjaxController;
use Zend\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],    
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'login',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'logout',
                    ],
                ],
            ],
            'passwordreset' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/wachtwoordherstel/:token',
                    'defaults' => array(
                        'controller' => Controller\IndexController::class,
                        'action' => 'reset',
                    ),
                ),
            ),            
            'contact' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/contact',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'contact',
                    ],
                ],
            ],        
            'userAjax' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/ajax',
                ),
                'may_terminate' => true,
                'child_routes' => array(
            
                    'validate-email' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/validateEmail',
                            'defaults' => array(
                                'controller' => UserAjaxController::class,
                                'action' => 'validateEmail',
                            ),
                        ),
                    ),
                    
                ),
            ),
            
            'authenticationAjax' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/ajax',
                    'defaults' => array(
                        'controller' => AuthenticationAjaxController::class,
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
            
                    'login' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'controller' => AuthenticationAjaxController::class,
                                'action' => 'getform',
                            ),
                        ),
                    ),
                    'authenticate' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/authenticate',
                            'defaults' => array(
                                'controller' => AuthenticationAjaxController::class,
                                'action' => 'authenticate',
                            ),
                        ),
                    ),
                    'authenticatereset' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/authenticatereset',
                            'defaults' => array(
                                'controller' => AuthenticationAjaxController::class,
                                'action' => 'AuthenticationResetMail',
                            ),
                        ),
                    ),
                    
                     'getresetform' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/getresetform',
                            'defaults' => array(
                                'controller' => AuthenticationAjaxController::class,
                                'action' => 'getresetform',
                            ),
                        ),
                    ),
                    
                    'AuthenticationReset' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/AuthenticationReset',
                            'defaults' => array(
                                'controller' => AuthenticationAjaxController::class,
                                'action' => 'AuthenticationReset',
                            ),
                        ),
                    ),
                ),
            ),
    
            'dashboard' => [
                'type' => 'Literal',
                'options' => [
                    // Change this to something specific to your module
                    'route' => '/dashboard',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action' => 'dashboard',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    // You can place additional routes that match under the
                    // route defined above here.
                ],
            ],
    
            'medewerker' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/medewerker',
                    'defaults' => array(
                        'controller' => Controller\MedewerkerController::class,
                        'action' => 'list',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'list' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/list',
                            'defaults' => array(
                                'controller' => Controller\MedewerkerController::class,
                                'action' => 'list',
                            ),
                        ),
                    ),
                    'view' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/bekijk/:id',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\MedewerkerController::class,
                                'action' => 'view',
                            ),
                        
                        
                        ),
                    ),
                    'add' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/nieuw[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\MedewerkerController::class,
                                'action' => 'add',
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/bewerk/:id',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\MedewerkerController::class,
                                'action' => 'edit',
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/verwijder[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\MedewerkerController::class,
                                'action' => 'delete',
                            ),
                        ),
                    ),
                
                ),
            ),
                
            'rollen' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/rollen',
                    'defaults' => array(
                        'controller' => Controller\RoleController::class,
                        'action' => 'list',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'list' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/list',
                            'defaults' => array(
                                'controller' => Controller\RoleController::class,
                                'action' => 'list',
                            ),
                        ),
                    ),
                    'view' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/bekijk/:id',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\RoleController::class,
                                'action' => 'view',
                            ),
                        
                        
                        ),
                    ),
                    'add' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/nieuw[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\RoleController::class,
                                'action' => 'add',
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/bewerk/:id',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\RoleController::class,
                                'action' => 'edit',
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/verwijder[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => Controller\RoleController::class,
                                'action' => 'delete',
                            ),
                        ),
                    ),
                ),
            ),            
        ],
    ],
];
