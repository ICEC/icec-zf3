<?php

namespace Application;

use Application\Controller\Authentication\AuthenticationAjaxController;

return [
    'circlical' => [
        'user' => [
            'guards' => [
                'ModuleName' => [
                    "controllers" => [
                        Controller\IndexController::class => [
                            'default' => [], // anyone can access
                        ],
                        AuthenticationAjaxController::class => [
                            'default' => [], // anyone can access
                        ],
                        Controller\DashboardController::class => [
                            'default' => ['Gebruiker'],
                        ],
                        Controller\RoleController::class => [
                            'default' => ['Admin'], // Admin can access
                        ],
                        Controller\MedewerkerController::class => [
//                            'default' => [], // anyone can access
                            'default' => ['Admin'], // Admin can access
                        ],
                    ],
                ],
            ],
        ],
    ],
];
