<?php

namespace Application;

use Application\Controller\AbstractAjaxController;
use Application\Controller\Authentication\AuthenticationAjaxController;
use Application\Controller\Authentication\Factory\AuthenticationAjaxControllerFactory;
use Application\Controller\Factory\AbstractAjaxControllerFactory;
use Application\Controller\Factory\AbstractControllerFactory;
use Application\Controller\Factory\RoleControllerFactory;
use Application\Controller\UserAjaxController;
use Application\Form\Element\Canvas;
use Application\Form\Factory\AbstractFieldsetFactory;
use Application\Form\Factory\AbstractFormFactory;
use Application\Form\Factory\MedewerkerFormFactory;
use Application\Form\Factory\RoleFormFactory;
use Application\Listener\RoleLoadClassMetadataSubscriber;
use Application\Service\AbstractPDFMailService;
use Application\Service\Authentication\UserAuthService;
use Application\Service\Factory\AbstractEntityServiceFactory;
use Application\Service\Factory\AbstractPDFMailServiceFactory;
use Application\Service\Factory\GroupPermissionFactory;
use Application\Service\Factory\MailServiceFactory;
use Application\Service\Factory\UserAuthServiceFactory;
use Application\Service\GroupPermission;
use Application\Service\Medewerker;
use Application\View\Helper\Factory\FetchAccessServiceViewHelperFactory;
use Application\View\Helper\FormDateRowSelectHelper;
use Application\View\Helper\FormSelectDateTimePartialHelper;
use Application\View\Helper\FormTimeRowSelectHelper;
use Application\View\Helper\FoundationFormLabelHelper;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\I18n\Translator\Resources;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => AbstractControllerFactory::class,
            Controller\DashboardController::class => AbstractControllerFactory::class,
            AuthenticationAjaxController::class => AuthenticationAjaxControllerFactory::class,
            UserAjaxController::class => AbstractAjaxControllerFactory::class,
            Controller\MedewerkerController::class => AbstractControllerFactory::class,
            Controller\RoleController::class => RoleControllerFactory::class,
            AbstractAjaxController::class => AbstractAjaxControllerFactory::class,
        
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\IndexService::class => AbstractEntityServiceFactory::class,
            Medewerker::class => AbstractEntityServiceFactory::class,
            Service\MailService::class => MailServiceFactory::class, // TODO: why wont you register this as an abstract factory?
            UserAuthService::class => UserAuthServiceFactory::class,
            GroupPermission::class => GroupPermissionFactory::class,
            
            // Custom fieldsets
            Form\AuthForm::class => InvokableFactory::class,
            Form\RoleForm::class => RoleFormFactory::class,
            Form\MedewerkerForm::class => MedewerkerFormFactory::class,
            
            AbstractPDFMailService::class => AbstractPDFMailServiceFactory::class,
        ],
        'invokables' => [
            RoleLoadClassMetadataSubscriber::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            
            //Off Canvas Navigation
            'application/navigation/off-canvas-nav' => __DIR__ . '/../view/application/navigation/off-canvas-nav.phtml',
            
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'user/403' => __DIR__ . '/../view/error/geen_recht.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            
            //Authentication Ajax Partials
            'application/authentication/login_modal' => __DIR__ . '/../view/application/authentication/login_modal.phtml',
            'application/authentication/reset_modal' => __DIR__ . '/../view/application/authentication/reset_modal.phtml',
            
            'form/modal/modalformelement' => __DIR__ . '/../view/form/modal/modalformelement.phtml',
            'form/modal/modalsubmitinrow' => __DIR__ . '/../view/form/modal/modalsubmitinrow.phtml',
            
            'form/element/stacked_td_form_element_partial' => __DIR__ . '/../view/form/element/stacked_td_form_element_partial.phtml',
            'form/element/handtekeningcanvas' => __DIR__ . '/../view/form/element/handtekeningcanvas.phtml',
            'form/element/datetimeselect' => __DIR__ . '/../view/form/element/datetimeselect.phtml',
            
            'form/partials/foundationelement' => __DIR__ . '/../view/form/partials/foundationelement.phtml',
            'form/partials/foundationdateelement' => __DIR__ . '/../view/form/partials/foundationdateelement.phtml',
            
            'form/partials/foundationshowtextelement' => __DIR__ . '/../view/form/partials/foundationshowtextelement.phtml',
            'form/partials/foundationshowdatetelement' => __DIR__ . '/../view/form/partials/foundationshowdateelement.phtml',
            'form/partials/foundationshowtextareaelement' => __DIR__ . '/../view/form/partials/foundationshowtextareaelement.phtml',
            'partials/headerbreadcrum' => __DIR__ . '/../view/partials/headerbreadcrum.phtml',
            'partials/view/headerbreadcrum' => __DIR__ . '/../view/partials/view/headerbreadcrum.phtml',
            'partials/view/loading_wheel' => __DIR__ . '/../view/partials/view/loading_wheel.phtml',
            
            'partials/tab_panel_partial' => __DIR__ . '/../view/partials/tab_panel_partial.phtml',
            'partials/view/view_handtekening_image' => __DIR__ . '/../view/partials/view/view_handtekening_image.phtml',
            'partials/view/stacked_td_partial_centerd_text' => __DIR__ . '/../view/partials/view/stacked_td_partial_centerd_text.phtml',
            'partials/view/stacked_td_partial' => __DIR__ . '/../view/partials/view/stacked_td_partial.phtml',
            'partials/view/stacked_td_partial_center_text' => __DIR__ . '/../view/partials/view/stacked_td_partial_center_text.phtml',
            'partials/view/stacked_td_partial_url' => __DIR__ . '/../view/partials/view/stacked_td_partial_url.phtml',
            'partials/view/stacked_td_partial_date' => __DIR__ . '/../view/partials/view/stacked_td_partial_date.phtml',
            'partials/view/stacked_td_partial_date_time' => __DIR__ . '/../view/partials/view/stacked_td_partial_date_time.phtml',
            'partials/view/stacked_td_partial_year_month_date' => __DIR__ . '/../view/partials/view/stacked_td_partial_year_month_date.phtml',
            'partials/view/stacked_td_partial_smal_medium_only' => __DIR__ . '/../view/partials/view/stacked_td_partial_smal_medium_only.phtml',
            'partials/view/stacked_td_partial_sorter_row' => __DIR__ . '/../view/partials/view/stacked_td_partial_sorter_row.phtml',
            'partials/view/view_label_cleartextvalue' => __DIR__ . '/../view/partials/view/view_label_cleartextvalue.phtml',
        
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],
    'view_helpers' => array(
        'factories' => array(
            'FetchAccessService' => FetchAccessServiceViewHelperFactory::class,
        ),
        'aliases' => array(
            'FoundationFormLabel' => FoundationFormLabelHelper::class,
        ),
        'invokables' => array(
            'FormSelectDateTimePartialHelper' => FormSelectDateTimePartialHelper::class,
            'FormDateRowSelectHelper' => FormDateRowSelectHelper::class,
            'FormTimeRowSelectHelper' => FormTimeRowSelectHelper::class,
        ),
    ),
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    RoleLoadClassMetadataSubscriber::class,
                ],
            ],
        ],
    ],
    'form_elements' => [
        'abstract_factories' => [
            AbstractFormFactory::class,
            AbstractFieldsetFactory::class,
        ],
        'invokables' => [
            'canvas' => Canvas::class,
        ],
    ],
    'translator' => [
        'locale' => 'nl',
        'translation_file_patterns' => [
            [
                'text_domain' => 'nl',
                'type' => 'phpArray',
                'base_dir' => Resources::getBasePath(),
                'pattern' => Resources::getPatternForValidator(),
            ],
        ],
    ],
];
