<?
namespace Application\Form;

use Application\Common\CsrfFieldsetTrait;
use Application\Common\EntityClassIncludeTrait;
use Application\Common\FieldsetHydratorTrait;
use Application\Common\IdFieldsetTrait;
use Application\Common\ObjectManagerForElementsTrait;
use Application\Common\ObjectManagerIncludeTrait;
use Application\Common\ServiceManagerIncludeTrait;
use Zend\Form\Fieldset;
use Zend\Form\FieldsetInterface;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class AbstractFieldset
 *
 * @package Application\Form
 */
abstract class AbstractFieldset extends Fieldset implements FieldsetInterface, InputFilterProviderInterface {
    
    use ObjectManagerIncludeTrait;
    use ServiceManagerIncludeTrait;
    use EntityClassIncludeTrait;
    use IdFieldsetTrait;
    use CsrfFieldsetTrait;
    use FieldsetHydratorTrait;
    use ObjectManagerForElementsTrait;
    
    /**
     *
     */
    public function init() {
        $this
            ->setObject(new $this->entityClass())
            ->setOptions(array('target_class' => $this->entityClass));
        $this->addHiddenIdElement($this);
    }
    
    /**
     * @var
     */
    protected $fieldset_name;
    
    /**
     * @return mixed
     */
    public function getFieldsetName() {
        return $this->fieldset_name;
    }
   
    public function setFieldsetName() {
        $this->fieldset_name = strtolower((new \ReflectionClass($this->getEntityClass()))->getShortName());
    }
    
}