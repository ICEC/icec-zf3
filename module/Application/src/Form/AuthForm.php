<?php
namespace Application\Form;

use Application\Common\BaseModalFormElements;
use Application\Common\FieldsetHydratorTrait;
use Application\Common\ObjectManagerIncludeTrait;
use Application\Entity\User;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Hostname;

class AuthForm extends Form {
    use FieldsetHydratorTrait;
    use BaseModalFormElements;
    use ObjectManagerIncludeTrait;
    protected $entityClass = User::class;
    
    /**
     * Constructor.
     */
    public function init()
    {
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('data-form', 'postform');
        $this->addElements();
        $this->addBaseModalFormElement($this);
        $inputFilter = $this->addInputFilter();
        $this->addBaseModalFormElementInputFilters($inputFilter);

    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'email',
            'name' => 'email',
            'attributes' => [
                'placeholder' => 'Email',
                'required' => true,
                'pattern' => 'email'
            ],
            'options' => [
                'label' => 'Email',
                'label_options' => [
                ]
            ],
        ]);

        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'placeholder' => 'Wachtwoord',
                'required' => true,
            ],
            'options' => [
                'label' => 'Wachtwoord',
                'label_options' => [
                ]
            ],
        ]);
        $this->add([
            'type'  => 'checkbox',
            'name' => 'vergeten',
            'attributes' => [
                'placeholder' => 'Password',
                'required' => false,
            ],
            'options' => [
                'label' => 'Wachtwoord vergeten ?',
                'label_options' => [
                ]
            ],
        ]);
        
        $this->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);
        $this->add([
            'type'  => 'hidden',
            'name' => 'token',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);

        $this->add([
            'type'  => 'button',
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'class' => 'button expanded',
            ],
            'options' => [
                'label' => 'Login',
            ],
        ]);

    }

    // This method creates input filter (used for form filtering/validation).
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
                'name'     => 'email',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'EmailAddress',
                        'options' => [
                            'allow' => Hostname::ALLOW_DNS,
                            'useMxCheck' => false,
                        ],
                    ],
                ],
            ]
        );

        //TODO add more password entry validation
        $inputFilter->add([
                'name'     => 'password',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'min' => 6,
                            'max' => 32
                        ],
                    ],
                ],
            ]
        );
        $inputFilter->add([
                'name'     => 'vergeten',
                'required' => false,
            ]
        );
        $inputFilter->add([
                'name'     => 'token',
                'required' => false,
            ]
        );
        return $inputFilter;
    }
}