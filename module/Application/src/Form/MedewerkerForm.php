<?php

namespace Application\Form;

use Application\Common\CsrfAndSubmitFieldsetTrait;
use Application\Common\FieldsetHydratorTrait;
use Application\Common\ObjectManagerForElementsTrait;
use Application\Entity\Medewerker;
use CirclicalUser\Entity\Role;
use DoctrineModule\Form\Element\ObjectSelect;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Hostname;

class MedewerkerForm extends AbstractForm {
    
    use CsrfAndSubmitFieldsetTrait, FieldsetHydratorTrait, ObjectManagerForElementsTrait;
    
    protected $entityClass = Medewerker::class;
    
    
    public function init() {
        $this->setAttribute('method', 'post');
        $this->setAttribute('data-form', 'postform');
        
        // add elements defined here
        $this->addElements();
        $this->addInputFilter();
        
        // Add csrf and submit button from trait
        $this->setFieldSetHydrator($this, $this->entityClass, $this->objectManager);
        $this->setObjectManagerForElements($this, $this->objectManager);
        
        /** @var ObjectSelect $rolesElement */
        $rolesElement = $this->get('roles');
        $rolesElement->setDisableInArrayValidator(true);
        
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() {
        $this->add([
            'type' => 'email',
            'name' => 'email',
            'attributes' => [
                'placeholder' => 'Email van medewerker',
                'required' => true,
                //                'pattern' => 'email'
            ],
            'options' => [
                'label' => 'Email',
                'label_options' => [
                    'class' => 'text-right middle',
                ],
            ],
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'voornaam',
            'attributes' => [
                'placeholder' => 'Voornaam van medewerker',
                'required' => true,
            ],
            'options' => [
                'label' => 'Voornaam',
                'label_options' => [
                    'class' => 'text-right middle',
                ],
            ],
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'achternaam',
            'attributes' => [
                'placeholder' => 'Achternaam van medewerker',
                'required' => true,
            ],
            'options' => [
                'label' => 'Achternaam',
                'label_options' => [
                    'class' => 'text-right middle',
                ],
            ],
        ]);
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'roles',
            'registerInArrayValidator' => false,
            'options' => array(
                'required' => true,
                'disable_inarray_validator' => true,
                'label' => 'Gebruiker rol',
                'label_options' => [
                    'class' => 'text-right middle',
                ],
                'object_manager' => $this->objectManager,
                'target_class' => Role::class,
                'display_empty_item' => false,
                
                'label_generator' => function ($targetEntity) {
                    /** @var Role $targetEntity */
                    return $targetEntity->getName();
                },
                'property' => 'id',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'getMedewerkerRoles',
                    //                    'params' => array(
                    //                        'criteria' => array(),
                    //                        'exclude' => [
                    //                            'field' => 'id',
                    //                            'value' => array(2,3,4)
                    //                        ],
                    ////                        'criteria' => array('id' => [2,3,4,5,6,7]),
                    //                        'orderBy' => array('name' => 'ASC'),
                    //                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'singleSelect',
                'multiple' => true,
                'required' => true,
            ),
        ));
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);
        
    }
    
    // This method creates input filter (used for form filtering/validation).
    private function addInputFilter() {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        $inputFilter->add([
                'name' => 'email',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                    ['name' => 'StripNewlines'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty'],
                    [
                        'name' => 'EmailAddress',
                        'options' => [
                            'allow' => Hostname::ALLOW_DNS,
                            'useMxCheck' => false,
                            'useDeepMxCheck' => false,
                        ],
                    ],
                ],
            ]
        );
        $inputFilter->add([
                'name' => 'voornaam',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                    ['name' => 'StripNewlines'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty'],
                ],
            ]
        );
        $inputFilter->add([
                'name' => 'achternaam',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                    ['name' => 'StripNewlines'],
                ],
                'validators' => [
                    ['name' => 'NotEmpty'],
                ],
            ]
        );
        $inputFilter->add([
                'name' => 'roles',
                'required' => true,
                'allow_empty' => false,
                'continue_if_empty' => false,
                'validators' => [
                    ['name' => 'NotEmpty'],
                ],
            ]
        );
    }
}