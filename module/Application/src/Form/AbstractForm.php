<?php
namespace Application\Form;

use Application\Common\CsrfFieldsetTrait;
use Application\Common\EntityClassIncludeTrait;
use Application\Common\FieldsetHydratorTrait;
use Application\Common\IdFieldsetTrait;
use Application\Common\ObjectManagerForElementsTrait;
use Application\Common\ObjectManagerIncludeTrait;
use Application\Common\ServiceManagerIncludeTrait;
use Zend\Form\Form;
/**
 * Class AbstractForm
 *
 * @package Application\Form
 */
class AbstractForm extends Form {
    
    use CsrfFieldsetTrait;
    use IdFieldsetTrait;
    use ObjectManagerIncludeTrait;
    use ServiceManagerIncludeTrait;
    use EntityClassIncludeTrait;
    use FieldsetHydratorTrait;
    use ObjectManagerForElementsTrait;
    
    /**
     * AbstractForm constructor.
     *
     * @param null $name
     * @param array $options
     */
    public function __construct($name = null, $options = array()) {
        $this->setObject(new $this->entityClass());
        parent::__construct($name, $options);
    }
    
    /**
     *
     */
    public function init() {
        $this->setAttribute('method', 'post');
        $this->setAttribute('data-form', 'postform');
        $this->addHiddenIdElement($this);
        $this->addCsrf($this);
    }
}
