<?php
namespace Application\Form;

use Beheer\Entity\Algemeen\Status;
use CirclicalUser\Entity\Role;
use DoctrineModule\Form\Element\ObjectSelect;
use Zend\Filter\StringTrim;
use Zend\Filter\ToNull;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputFilter;

class RoleForm extends AbstractForm {
    
    protected $entityClass = Role::class;

    public function init()
    {
        $this->setAttribute('method', 'post');
        $this->setAttribute('data-form', 'postform');

        // add elements defined here
        $this->addElements();
        $this->addInputFilter();

        // Add csrf and submit button from trait
        $this->setFieldSetHydrator($this,$this->entityClass,$this->objectManager);
        $this->setObjectManagerForElements($this,$this->objectManager);
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => Text::class,
            'name' => 'name',
            'attributes' => [
                'placeholder' => 'Rol Naam',
                'required' => true,
            ],
            'options' => [
                'label' => 'Rol',
                'label_options' => [
                    'class' => 'text-right middle'
                ]
            ],
        ]);

        $this->add(array(
            'type' => ObjectSelect::class,
            'name' => 'parent',
            'required' => false,
            'options' => array(
                'required' => false,
                'label' => 'Bovenliggende rol',
                'label_options' => [
                    'class' => 'text-right middle'
                ],
                'object_manager' => $this->objectManager,
                'target_class' => $this->entityClass,
                'display_empty_item' => true,
                'empty_item_label'   => 'Kies bovenliggende rol',

                'disable_inarray_validator' => true,
                'allow_empty' => true,
                'label_generator' => function ($targetEntity) {
                    /** @var Role $targetEntity */
                    return $targetEntity->getName();
                },
                'option_attributes' => array(
                    'data-entity' => function ($targetEntity) {
                        /** @var Role $targetEntity */
                        return (new \ReflectionClass($targetEntity))->getName();
                    },
                ),
                'property' => 'id',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'findAllExcludeSelf',
                    'params' => array(
                        'form' => $this,
                        'orderBy' => array('name' => 'ASC'),
                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'singleSelect',
                'multiple' => false,
                'required' => false
            ),
        ));
    
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectMultiCheckbox',
            'name' => 'status_for_role',
            'required' => false,
            'options' => array(
                'required' => false,
                'label' => 'Status beschikbaar voor rol (min 1)',
                'label_options' => [
                    'class' => 'text-right middle',
                ],
                'object_manager' => $this->objectManager,
                'target_class' => Status::class,
                'disable_inarray_validator' => true,
                'allow_empty' => true,
                'label_generator' => function ($targetEntity) {
                    /** @var Status $targetEntity */
                    return $targetEntity->getStatus();
                },
                'property' => 'id',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('volgnummer' => 'ASC'),
                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'singleSelect',
                'multiple' => true,
                'required' => false,
            ),
        ));
        $this->add([
            'type'  => Csrf::class,
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);
    }

    // This method creates input filter (used for form filtering/validation).
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => StringTrim::class],
                ],
            ]
        );
        $inputFilter->add([
                'name'     => 'parent',
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'filters'  => [
                    [
                        'name' => ToNull::class,
                        'options' =>
                            ['type' => ToNull::TYPE_ALL]
                    ],
                ],
            ]
        );
        $inputFilter->add([
                'name'     => 'status_for_role',
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'filters'  => [
                    [
                        'name' => ToNull::class,
                        'options' =>
                            ['type' => ToNull::TYPE_ALL]
                    ],
                ],
            ]
        );
    }
}