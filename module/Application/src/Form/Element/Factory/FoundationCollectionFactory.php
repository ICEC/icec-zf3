<?php

namespace Application\Form\Element\Factory;

//use Zend\Di\ServiceLocator;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FoundationCollectionFactory {

    /**
     * Determine if we can create a service with name
     *
     * @param ServiceLocatorInterface $serviceManager
     * @param $name
     * @param $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceManager, $name, $requestedName) {
//        $reflector = new \ReflectionClass($requestedName);
//        return array_key_exists('LosBase\Entity\BaseEntityInterface', $reflector->getInterfaces());
        if (!class_exists($requestedName)) {
            return false;
        }     
        return true;
    }
    
    
    /**
     * Create service with name
     *
     * @param ServiceLocatorInterface $serviceManager
     * @param $name
     * @param $requestedName
     * @return mixed
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceManager, $name, $requestedName) {
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($requestedName);

        // sets the fieldsets object
        if (!empty($_POST) || self::hasPRGDataInSession()) {
            if (!(new \ReflectionClass($requestedName))->isAbstract()) {
                $form->setObject(new $requestedName());
            }
        }

        return $form;
    }

    private static function hasPRGDataInSession() {
        foreach ($_SESSION as $key => $value) {
            $pos = strpos($key, "file_prg_post");
            if ($pos === 0 && $value->__isset('post')) {
                return true;
            }
        }
        return false;
    }
}