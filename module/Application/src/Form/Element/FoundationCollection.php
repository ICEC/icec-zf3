<?php
namespace Application\Form\Element;

//use LosBase\Form\Element\Factory\SITCollectionFactory;
use Application\Form\Element\Factory\FoundationCollectionFactory;
use Traversable;
use Zend\Form\Element;
use Zend\Form\Element\Collection;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Stdlib\ArrayUtils;

class FoundationCollection extends Collection
{

    /**
     * @inheritdoc
     */
    public function setTargetElement($elementOrFieldset)
    {
        if (is_array($elementOrFieldset)
            || ($elementOrFieldset instanceof Traversable && !$elementOrFieldset instanceof ElementInterface)
        ) {
            $factory = $this->getFormFactory();
            $factory->getFormElementManager()->addAbstractFactory(FoundationCollectionFactory::class);
            $elementOrFieldset = $factory->create($elementOrFieldset);
        }

        if (!$elementOrFieldset instanceof ElementInterface) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that $elementOrFieldset be an object implementing %s; received "%s"',
                __METHOD__,
                __NAMESPACE__ . '\ElementInterface',
                (is_object($elementOrFieldset) ? get_class($elementOrFieldset) : gettype($elementOrFieldset))
            ));
        }

        $this->targetElement = $elementOrFieldset;

        return $this;
    }
}
