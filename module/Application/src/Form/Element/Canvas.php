<?php

namespace Application\Form\Element;

use Zend\Form\Element\Hidden;

/**
 * Class Canvas
 *
 * @package Application\Form\Element
 */
class Canvas extends Hidden {
    /**
     * @var array
     */
    protected $attributes = [
        'type' => 'canvas',
    ];
}