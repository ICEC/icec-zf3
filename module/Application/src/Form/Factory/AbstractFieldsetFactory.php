<?php
namespace Application\Form\Factory;

use Application\Form\AbstractFieldset;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

class AbstractFieldsetFactory implements AbstractFactoryInterface
{

    
    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $reflectionClass = new \ReflectionClass($requestedName);
        return $reflectionClass->isSubclassOf(AbstractFieldset::class);
    }
    
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $objectManager = $container->get(EntityManager::class);
        /** @var AbstractFieldset $instance */
        $instance = new $requestedName(
        //            $objectManager
        );
        $instance->setObjectManager($objectManager);
        $instance->setServiceManager($container);
        return $instance;
    }
    
}