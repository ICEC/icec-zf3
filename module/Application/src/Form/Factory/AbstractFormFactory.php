<?php

namespace Application\Form\Factory;

use Application\Form\AbstractForm;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

/**
 * Class AbstractFormFactory
 *
 * @package Applicatie\Form\Factory
 */
class AbstractFormFactory implements AbstractFactoryInterface
{

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $reflectionClass = new \ReflectionClass($requestedName);
        return $reflectionClass->isSubclassOf(AbstractForm::class);
    }

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $objectManager = $container->get(EntityManager::class);
        /** @var AbstractForm $instance */
        $instance = new $requestedName(
//            $objectManager
        );
        $instance->setObjectManager($objectManager);
        $instance->setServiceManager($container);
        return $instance;
    }
}