<?php
namespace Application\Form\Factory;

use Application\Form\RoleForm;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
/**
 * Class RoleFormFactory
 *
 * @package Application\Form\Factory
 */
class RoleFormFactory implements FactoryInterface {
    
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RoleForm
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        $entityManager = $container->get(EntityManager::class);
        return new RoleForm(
            $entityManager
        );
    }
}