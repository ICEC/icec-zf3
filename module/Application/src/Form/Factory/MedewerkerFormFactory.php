<?php
namespace Application\Form\Factory;

use Application\Form\MedewerkerForm;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
/**
 * Class MedewerkerFormFactory
 *
 * @package Application\Form\Factory
 */
class MedewerkerFormFactory implements FactoryInterface {
    
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return MedewerkerForm
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        $entityManager = $container->get(EntityManager::class);
        return new MedewerkerForm(
            $entityManager
        );
    }
}