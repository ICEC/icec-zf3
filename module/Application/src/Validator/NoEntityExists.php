<?php
namespace Application\Validator;

use Zend\Validator\Exception\InvalidArgumentException;
use DoctrineModule\Validator\NoObjectExists;
/**
 * Class NoEntityExists
 *
 * @package Application\Validator
 */
class NoEntityExists extends NoObjectExists {
    
    /**
     * @var mixed|null
     */
    private $additionalFields = null;
    
    /**
     * NoEntityExists constructor.
     *
     * @param array $options
     */
    public function __construct(array $options) {
        parent::__construct($options);
        
        if (isset($options['additionalFields'])) {
            $this->additionalFields = $options['additionalFields'];
        }
    }
    
    /**
     * @param mixed $value
     * @param null $context
     * @return bool
     */
    public function isValid($value, $context = null) {
        if (null !== $this->additionalFields && is_array($context)) {
            $value = (array)$value;
            foreach ($this->additionalFields as $field) {
                if (!isset($context[$field])) {
                    throw new InvalidArgumentException('Invalid Argument Exception');
                }
                $value[] = $context[$field];
            }
        }
        $value = $this->cleanSearchValue($value);
        $match = $this->objectRepository->findOneBy($value);
        
        if (is_object($match)) {
            if (is_array($value)) {
                $str = '';
                foreach ($value as $campo) {
                    if ($str != '') {
                        $str .= ', ';
                    }
                    $str .= $campo;
                }
                $value = $str;
            }
            $this->error(self::ERROR_OBJECT_FOUND, $value);            
            return false;
        }        
        return true;
    }
}
