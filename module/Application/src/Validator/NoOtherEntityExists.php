<?php
namespace Application\Validator;

use Zend\Validator\Exception\InvalidArgumentException;
use DoctrineModule\Validator\NoObjectExists;
/**
 * Class NoOtherEntityExists
 *
 * @package Application\Validator
 */
class NoOtherEntityExists extends NoObjectExists {
    
    /**
     * @var mixed
     */
    private $id;
    
    /**
     * @var mixed|null
     */
    private $additionalFields = null;
    
    /**
     * NoOtherEntityExists constructor.
     *
     * @param array $options
     */
    public function __construct(array $options) {
        parent::__construct($options);
        
        if (!isset($options['id'])) {
            throw new InvalidArgumentException('Invalid Argument Exception');
        }
        if (isset($options['additionalFields'])) {
            $this->additionalFields = $options['additionalFields'];
        }
        
        $this->id = $options['id'];
    }
    
    /**
     * @param mixed $value
     * @param null $context
     * @return bool
     */
    public function isValid($value, $context = null) {
        if (null !== $this->additionalFields && is_array($context)) {
            $value = (array)$value;
            foreach ($this->additionalFields as $field) {
                if (!isset($context[$field])) {
                    throw new InvalidArgumentException('Invalid Argument Exception');
                }
                $value[] = $context[$field];
            }
        }
        
        $value = $this->cleanSearchValue($value);
        $match = $this->objectRepository->findOneBy($value);
        
        if (is_object($match) && $match->getId() != $this->id) {
            if (is_array($value)) {
                $str = '';
                foreach ($value as $campo) {
                    if ($str != '') {
                        $str .= ', ';
                    }
                    $str .= $campo;
                }
                $value = $str;
            }
            $this->error(self::ERROR_OBJECT_FOUND, $value);
            
            return false;
        }
        
        return true;
    }
}
