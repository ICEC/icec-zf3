<?php

namespace Application\Controller\Authentication;

use Application\Controller\AbstractAjaxController;
use Application\Entity\User;
use Application\Form\AuthForm;
use Application\Service\Authentication\UserAuthService;
use Application\Service\MailService;
use CirclicalUser\Entity\Authentication;
use CirclicalUser\Entity\UserResetToken;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AuthenticationAjaxController
 *
 * @package Application\Controller\Authentication
 */
class AuthenticationAjaxController extends AbstractAjaxController {
    
    /**
     * AuthenticationAjaxController constructor.
     *
     * @param EntityManager $entityManager
     * @param AccessService $accessService
     * @param AuthenticationService $authenticationService
     * @param PhpRenderer $viewRender
     * @param FormElementManagerV3Polyfill $FormElementManager
     * @param MailService $mailService
     */
    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        PhpRenderer $viewRender,
        FormElementManagerV3Polyfill $FormElementManager,
        MailService $mailService
    
    ) {
        $this->entityManager = $entityManager;
        $this->accessService = $accessService;
        $this->authenticationService = $authenticationService;
        $this->viewRender = $viewRender;
        $this->FormElementManager = $FormElementManager;
        $this->mailService = $mailService;
    }
    
    /**
     * @var string
     */
    protected $entityClass = UserResetToken::class;
    
    /**
     * @var string
     */
    protected $entityServiceClass = UserAuthService::class;
    
    /**
     * @var
     */
    protected $entityFormClass = AuthForm::class;
    
    /**
     * @var MailService
     */
    protected $mailService;
    
    /**
     * @var string
     */
    protected $templatePath = 'application/authentication/login_modal';
    
    /**
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function getformAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->populateValues($request->getPost()->toArray());
            $this->setViewModelVars([
                'entityForm' => $entityForm,
                'authError' => false,
            ]);
        }
        return $this->returnpartialAction();
    }
    
    /**
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function authenticateAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        if ($request->isPost()) :
            $authError = false;
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->setData($request->getPost()->toArray());
            if ($entityForm->isValid()):
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $this->getEntityService();
                if ($authenticationService->TryAuthenticating($entityForm)) :
                    $entityForm->get('succes')->setValue(true);
                else:
                    $authError = true;
                endif;
            endif;
            $this->setViewModelVars([
                'entityForm' => $entityForm,
                'authError' => $authError,
            ]);
        endif;
        return $this->returnpartialAction();
    }
    
    /**
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function AuthenticationResetMailAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        $authError = false;
        if ($request->isPost()) :
            
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->get('password')->setAttribute('required', false);
            $entityForm->getInputFilter()->remove('password');
            $entityForm->setData($request->getPost()->toArray());
            
            if ($entityForm->isValid()):
                $authError = false;
                $userEntity = $this->getEntityManager()->getRepository(User::class)
                    ->findOneBy(['email' => $entityForm->get('email')->getValue()]);
                if (!is_object($userEntity)) {
                    $authError = true;
                }
                if (!$authError):
                    /** @var UserAuthService $authenticationService */
                    $authenticationService = $this->getEntityService();
                    $succes = $authenticationService->doPasswordReset($userEntity);
                    if ($succes) :
                        $entityForm->get('succes')->setValue(true);
                        $entityForm->get('email')->setValue($userEntity->getEmail());
                    
                    else:
                        $authError = $succes;
                    endif;
                endif;
            endif;
            $this->setViewModelVars([
                'entityForm' => $entityForm,
                'authError' => $authError,
            ]);
        endif;
        return $this->returnpartialAction();
    }
    
    /**
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function getresetformAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $this->setTemplatePath('application/authentication/reset_modal');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tokenError = true;
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->populateValues($request->getPost()->toArray());
            $resettoken = $this->getEntityManager()->getRepository(UserResetToken::class)
                ->findOneBy(['token' => $request->getPost('token'), 'status' => 0]);
            if ($resettoken instanceof UserResetToken) {
                /** @var UserResetToken $resettoken */
                /** @var Authentication $Authentication */
                $tokenError = false;
                $Authentication = $resettoken->getAuthentication();
                $userEntity = $this->getEntityManager()->getRepository(User::class)
                    ->find($Authentication->getUserId());
                if ($userEntity instanceof User) {
                    $entityForm->get('token')
                        ->setValue($request->getPost('token'));
                    $entityForm->get('email')
                        ->setValue($userEntity->getEmail())
                        ->setAttribute('readonly', true);
                }
            }
            $this->setViewModelVars([
                'entityForm' => $entityForm,
                'tokenError' => $tokenError,
                'authError' => false,
            ]);
        }
        return $this->returnpartialAction();
    }
    
    /**
     * Overriding the default getEntityClass cause were passing the Mailservice too
     *      should be at constructor do
     *
     * @return mixed
     */
    public function getEntityService() {
        if (null === $this->entityService) {
            $entityServiceClass = $this->getEntityServiceClass();
            if (!class_exists($entityServiceClass)) {
                throw new \RuntimeException("Class $entityServiceClass doesn't exist!");
            }
            $this->entityService = new $entityServiceClass(
                $this->getEntityManager(),
                $this->getAuthenticationService(),
                $this->getFormElementManager(),
                $this->mailService
            );
        }
        return $this->entityService;
    }
    
    /**
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function AuthenticationResetAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $this->setTemplatePath('application/authentication/reset_modal');
        $request = $this->getRequest();
        $authError = false;
        if ($request->isPost()) :
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->setData($request->getPost()->toArray());
            
            if ($entityForm->isValid()):
                $authError = false;
                $userEntity = $this->getEntityManager()->getRepository(User::class)
                    ->findOneBy(['email' => $entityForm->get('email')->getValue()]);
                if (!is_object($userEntity)) {
                    $authError = true;
                }
                if (!$authError):
                    $resettoken = $this->getEntityManager()
                        ->getRepository(UserResetToken::class)
                        ->findOneBy(['token' => $request->getPost('token'), 'status' => 0]);
                    $authenticationService = $this->getEntityService();
                    $succes = $authenticationService->doPasswordSet($resettoken, $request->getPost('password'));
                    if ($succes) :
                        $entityForm->get('succes')->setValue(true);
                        $entityForm->get('email')->setValue($userEntity->getEmail());
                    else:
                        $authError = $succes;
                    endif;
                endif;
            endif;
            $this->setViewModelVars([
                'entityForm' => $entityForm,
                'authError' => $authError,
                'tokenError' => false,
            ]);
        endif;
        return $this->returnpartialAction();
    }
}
    

