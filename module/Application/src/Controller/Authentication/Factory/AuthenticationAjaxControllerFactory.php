<?php
namespace Application\Controller\Authentication\Factory;

use Application\Controller\Authentication\AuthenticationAjaxController;
use Application\Controller\Factory\AbstractControllerFactory;
use Application\Service\MailService;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AuthenticationAjaxControllerFactory
 *
 * @package Magazijn\Controller\Factory
 */
class AuthenticationAjaxControllerFactory extends AbstractControllerFactory {
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return mixed
     */    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        $entityManager = $container->get(EntityManager::class);
        $entityManager->getFilters()->enable('soft-deleteable');

        return new AuthenticationAjaxController(
            $entityManager,
            $container->get(AccessService::class),
            $container->get(AuthenticationService::class),
            $container->get(PhpRenderer::class),
            $container->get('FormElementManager'),
            $container->get(MailService::class)
        );
    }
}