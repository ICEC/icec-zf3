<?

namespace Application\Controller;

use Application\Entity\User;
use Zend\View\Model\JsonModel;

/**
 * Class UserAjaxController
 *
 * @package Application\Controller
 */
class UserAjaxController extends AbstractAjaxController {
    
    /**
     * @var
     */
    protected $entityClass = User::class;
        
    /**
     * @var
     */
    protected $entityServiceClass = \Application\Service\User::class;
    
    /**
     * @return \Zend\Http\Response|JsonModel
     */
    public function validateEmailAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        if ($request->isPost()) {
            /** @var \Application\Service\User $userService */
            $userService = $this->getEntityService();
            return new JsonModel(
                $userService->ValidateInputedEmail(
                    $request->getPost('user_id'),
                    $request->getPost('email_input')
                )
            );
        }
        $response = $this->getResponse();
        return $response->setStatusCode(418);
    }
}
    

