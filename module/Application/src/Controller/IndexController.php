<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 *
 * @package Application\Controller
 */
class IndexController extends AbstractController {
    
    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function indexAction() {
        if ($this->getAuthenticationService()->hasIdentity()):
            return $this->redirect()->toRoute('dashboard');
        endif;
        return new ViewModel([
        ]);
    }
    
    /**
     * @return ViewModel
     */
    public function contactAction() {
        return new ViewModel();
    }
    
    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function loginAction() {
        if ($this->getAuthenticationService()->hasIdentity()):
            return $this->redirect()->toRoute('dashboard');
        endif;
        return new ViewModel([]);
    }
    
    /**
     * @return ViewModel
     */
    public function resetAction() {
        return new ViewModel([
            'token' => $this->getEvent()->getRouteMatch()->getParam('token', 0)
        ]);
    }
    
    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction() {
        $this->getAuthenticationService()->clearIdentity();
        return $this->redirect()->toRoute('home');
    }
}
