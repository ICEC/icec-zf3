<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
/**
 * Class DashboardController
 *
 * @package Application\Controller
 */
class DashboardController extends AbstractController {
    
    /**
     * @return ViewModel
     */
    public function dashboardAction() {
        
        return new ViewModel([
        ]);
    }
}
