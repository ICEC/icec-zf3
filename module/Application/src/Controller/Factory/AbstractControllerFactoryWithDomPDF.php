<?php
namespace Application\Controller\Factory;

use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;

class AbstractControllerFactoryWithDomPDF implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get(EntityManager::class);
        $entityManager->getFilters()->enable('soft-deleteable');

        return new $requestedName(
            $entityManager,
            $container->get(AccessService::class),
            $container->get(AuthenticationService::class),
            $container,
            $container->get('FormElementManager'),
            $container->get('dompdf'),
            $container->get(PhpRenderer::class)
        );
    }
}