<?php
namespace Application\Controller\Factory;

use Application\Service\MailService;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\View\Renderer\PhpRenderer;
/**
 * Class AbstractAjaxControllerFactory
 *
 * @package Magazijn\Controller\Factory
 */
class AbstractAjaxMailControllerFactory extends AbstractControllerFactory {
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return mixed
     */    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        $entityManager = $container->get(EntityManager::class);
        $entityManager->getFilters()->enable('soft-deleteable');
    
    
        $mailService = $container->get(MailService::class);
        
        return new $requestedName(
            $entityManager,
            $container->get(AccessService::class),
            $container->get(AuthenticationService::class),
            $container->get(PhpRenderer::class),
            $container->get('FormElementManager'),
            $mailService
        );
    }
}