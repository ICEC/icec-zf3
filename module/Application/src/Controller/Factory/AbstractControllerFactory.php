<?php
namespace Application\Controller\Factory;

use Application\Controller\AbstractController;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;
/**
 * Class AbstractControllerFactory
 *
 * @package Application\Controller\Factory
 */
class AbstractControllerFactory {
    
    /**
     * @var
     */
    private $reflectionClass;
    
    /**
     * Create service with name
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return AbstractController
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
        return $this->__invoke($serviceLocator, $requestedName);
    }
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        $serviceManager = $container->get(ServiceManager::class);
        $entityManager = $container->get(EntityManager::class);
        $entityManager->getFilters()->enable('soft-deleteable');
        
        return new $requestedName(
            $entityManager,
            $container->get(AccessService::class),
            $container->get(AuthenticationService::class),
            $serviceManager,
            $serviceManager->get('FormElementManager')
        );
    }
    
    /**
     * Determine if we can create a service with name
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
        return $this->canCreate($serviceLocator, $requestedName);
    }
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName) {
        if (!class_exists($requestedName)) {
            return false;
        }
        return true;
    }
    
    /**
     * @param $suffix
     * @return string
     */
    protected function getConventionalClass($suffix) {
        $suffix = ucfirst($suffix);
        $reflectionClass = $this->getReflectionClass();
        $nameSpace = str_replace('Controller', '', $reflectionClass->getNamespaceName()) . $suffix;
        $shortName = str_replace('Controller', '', $reflectionClass->getShortName()) . $suffix;
        return $nameSpace . '\\' . $shortName;
    }
    
    /**
     * @return \ReflectionClass
     */
    protected function getReflectionClass() {
        return $this->reflectionClass;
    }
    
    /**
     * @param mixed $reflectionClass
     */
    public function setReflectionClass($reflectionClass) {
        $this->reflectionClass = $reflectionClass;
    }

}