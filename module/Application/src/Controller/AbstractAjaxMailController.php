<?php

namespace Application\Controller;

use Application\Service\MailService;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AbstractAjaxController
 *
 * @package Magazijn\Controller
 */
class AbstractAjaxMailController extends AbstractAjaxController {
    
    protected $mailService;
    
    /**
     * AbstractAjaxController constructor.
     *
     * @param EntityManager $entityManager
     * @param AccessService $accessService
     * @param AuthenticationService $authenticationService
     * @param PhpRenderer $viewRender
     * @param FormElementManagerV3Polyfill $FormElementManager
     * @param  $mailService
     */
    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        PhpRenderer $viewRender,
        FormElementManagerV3Polyfill $FormElementManager,
         $mailService
    
    ) {
        $this->entityManager = $entityManager;
        $this->accessService = $accessService;
        $this->authenticationService = $authenticationService;
        $this->viewRender = $viewRender;
        $this->FormElementManager = $FormElementManager;
        $this->mailService = $mailService;
        parent::__construct($entityManager, $accessService, $authenticationService, $viewRender, $FormElementManager);
    }
    
    /**
     * @return MailService
     */
    public function getMailService() {
        return $this->mailService;
    }
    
    /**
     * @param $mailService
     */
    public function setMailService($mailService) {
        $this->mailService = $mailService;
    }
}
