<?php
namespace Application\Controller;

use Application\Common\Entity\EntityServicesTraits;
use Application\Common\EntityClassIncludeTrait;
use Application\Common\FormElementManagerIncludeTrait;
use Application\Entity\AbstractEntity;
use Application\Form\AbstractFieldset;
use Application\Form\AbstractForm;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AbstractAjaxController
 *
 * @package Magazijn\Controller
 */
class AbstractAjaxController extends AbstractRestfulController {
    use EntityServicesTraits;
    use EntityClassIncludeTrait;
    use FormElementManagerIncludeTrait;
    #region Protected Vars
    /**
     * @var
     */
    protected $entityForm;
    
    /**
     * @var
     */
    protected $entityService;
    
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var AccessService
     */
    protected $accessService;
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;
    /**
     * @var PhpRenderer
     */
    protected $viewRender;
    
    /**
     * @var
     */
    protected $entityFormClass;
    /**
     * @var
     */
    protected $viewModelVars;
    /**
     * @var
     */
    protected $templatePath;
    
    #endregion
    
    /**
     * AbstractAjaxController constructor.
     *
     * @param EntityManager $entityManager
     * @param AccessService $accessService
     * @param AuthenticationService $authenticationService
     * @param PhpRenderer $viewRender
     * @param FormElementManagerV3Polyfill $FormElementManager
     */
    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        PhpRenderer $viewRender,
        FormElementManagerV3Polyfill $FormElementManager
    
    ) {
        $this->entityManager = $entityManager;
        $this->accessService = $accessService;
        $this->authenticationService = $authenticationService;
        $this->viewRender = $viewRender;
        $this->FormElementManager = $FormElementManager;
    }
    
    /**
     * @return mixed
     */
    public function viewEntityAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $entityRepo = $this->getEntityManager()->getRepository($this->getEntityClass());
            $this->setViewModelVars([
                'entity' => $entityRepo->find($request->getPost('id')),
            ]);
        }
        return $this->returnpartialAction();
    }
    
    /**
     *  Abstract Return of partial,
     *      required setTemplatePath
     *      required setViewModelVars
     * 
     * @return AbstractAjaxController|Response|ViewModel
     */
    public function returnpartialAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $viewmodel = new ViewModel($this->getViewModelVars());
            $viewmodel->setTemplate($this->getTemplatePath());
            $viewmodel->setTerminal(true);
            return $viewmodel;
        }
        $response = $this->getResponse();
        return $response->setStatusCode(418);
    }
    
    /**
     * @return AbstractAjaxController|Response|ViewModel
     */
    public function getformAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $entityForm->populateValues($request->getPost()->toArray());
            $this->setViewModelVars([
                'entityForm' => $entityForm,
            ]);
        }
        return $this->returnpartialAction();
    }
    
    //TODO replace the useage of updateFormLabels with updateForm
    /**
     * @param $form
     * @param $entity
     * @return mixed
     */
    public function updateFormLabels($form, $entity) {
        /** @var AbstractForm $form */
        /** @var AbstractEntity $entity */
        if ($entity->getId() > 0) {
            $form->bind($entity);
            $form->get('action')->setValue('edit');
            $form->get('submit')->setLabel('Muteren');
            return $form;
        } else {
            $form->get('action')->setValue('add');
            $form->get('submit')->setLabel('Aanmaken');
            return $form;
        }
    }
    
    /**
     * @param $form
     * @param $entity
     * @return mixed
     */
    public function updateForm($form, $entity) {
        /** @var AbstractForm $form */
        if (is_object($entity)) {
            $form->bind($entity);
            $form->get('action')->setValue('edit');
            $form->get('submit')->setLabel('Bewerken');
            return $form;
        } else {
            $form->get('action')->setValue('add');
            $form->get('submit')->setLabel('Opslaan');
            return $form;
        }
    }
    
    /**
     *       Find entity if exist and bind to form
     *
     * @param $key
     * @return mixed
     */
    public function getUpdatedEntityForm($key) {
        return $this->updateForm(
            $this->getFormElementManager()->get($this->getEntityFormClass()
            ),
            $this->getEntityManager()->getRepository($this->getEntityClass())
                ->findOneBy(['id' => $key]));
    }
    
    /**
     *  Abstract Ajax persist for ModalForms
     *
     * @return AbstractAjaxController|\Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function submitformAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $entityForm = $this->getUpdatedEntityForm($request->getPost('id'));
            $data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $entityForm->setData($data);
            $persistedEntity = $this->getEntityService()->persistForm($entityForm);
            if (is_object($persistedEntity)) {
                /** @var AbstractEntity $persistedEntity*/
                $entityForm->get('succes')->setValue(true);
                $entityForm->get('id')->setValue($persistedEntity->getId());
            }
            $this->setViewModelVars([
                'entityForm' => $entityForm,
            ]);
        }
        return $this->returnpartialAction();
    }

    /**
     * @param $entity
     */
    protected function doHardDelete($entity){
        /** @var AbstractEntity $entity */
        $entity->setDeletedAt(new \DateTime());
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
        return;
    }
    
    /**
     * @return \Zend\Http\Response|JsonModel
     */
    public function deleteAction() {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->doHardDelete(
                $this->getEntityManager()
                    ->getRepository($this->getEntityClass())
                    ->findOneBy([
                        'id' => $request->getPost('id'),
                    ]));
            return new JsonModel(['succes' => true]);
        }
        $response = $this->getResponse();
        return $response->setStatusCode(418);
    }
    
    /**
     *  Returns a name prepaired fieldset of a form.
     *
     * @param $formClass
     * @param $fieldsetClass
     * @param $fieldsetName
     * @param $key
     * @return mixed
     */
    public function getFieldsetOfForm($formClass, $fieldsetClass, $fieldsetName, $key) {
        /** @var AbstractForm $form */
        /** @var AbstractFieldset $fieldset */
        $form = $this->getFormElementManager()->get($formClass);
        $fieldset = $this->getFormElementManager()->get($fieldsetClass);
        $fieldset->setAttribute('name', $key);
        $form->get($fieldsetName)->add($fieldset);
        $form->prepare();
        $returnFieldset = $form->get($fieldsetName)->get($key);
        return $returnFieldset;
    }
    
    #region Getters & Setters
    /**
     * @return PhpRenderer
     */
    public function getViewRender() {
        return $this->viewRender;
    }
    
    /**
     * @return AccessService
     */
    public function getAccessService() {
        return $this->accessService;
    }
    
    /**
     * @return mixed
     */
    public function getViewModelVars() {
        return $this->viewModelVars;
    }
    
    /**
     * @param mixed $viewModelVars
     */
    public function setViewModelVars($viewModelVars) {
        $this->viewModelVars = $viewModelVars;
    }
    
    /**
     * @return mixed
     */
    public function getTemplatePath() {
        return $this->templatePath;
    }
    
    /**
     * @param mixed $templatePath
     */
    public function setTemplatePath($templatePath) {
        $this->templatePath = $templatePath;
    }
    #endregion
}
