<?php

namespace Application\Controller;

use Application\Form\AbstractForm;
use Application\Form\RoleForm;
use Application\Service\GroupPermission;
use CirclicalUser\Entity\Role;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\Select;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\Response;
use Zend\ServiceManager\ServiceManager;

/**
 * Class RoleController
 *
 * @package Application\Controller
 */
class RoleController extends AbstractController
{

    #region Protected Vars

    /**
     * @var string
     */
    protected $afterPostRedirectToUrl = 'rollen';

    /**
     * @var
     */
    protected $entityClass = Role::class;

    /**
     * @var
     */
    protected $entityServiceClass = \Application\Service\Role::class;

    /**
     * @var
     */
    protected $entityFormClass = RoleForm::class;

    /** @var GroupPermission */
    protected $groupPermission;

    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        ServiceManager $serviceManager,
        FormElementManagerV3Polyfill $FormElementManager,
        GroupPermission $groupPermission
    ) {
        parent::__construct($entityManager, $accessService, $authenticationService, $serviceManager, $FormElementManager);
        $this->groupPermission = $groupPermission;
    }

    #endregion

    /**
     * TODO: refactor
     *
     * @return array|bool|Response
     */
    public function editAction()
    {
        $result = parent::editAction();
        if ($result instanceof Response && $result->getStatusCode() === Response::STATUS_CODE_302) {
            return $result; // CSRF/timeout redirect
        }

        /** @var AbstractForm $form */
        $form = $this->getEntityForm();
        /** @var Role $entity */
        $entity = $form->getObject();
        $permissionsService = $this->groupPermission;

        # region handle post
        if ($result instanceof Response && $result->getStatusCode() === Response::STATUS_CODE_303) {
            /** @var Request $request */
            $request = $this->getRequest();
            $allowed = $request->getPost('rolePermissions', []);
            $denied = $request->getPost('sortedGuardRules', []);
            $permissionsService->persistPermissionChanges($entity, $allowed, $denied);
            return $result;
        }
        # endregion

        // 1. Create allowed multi-select
        # region allowed
        $rolePermissions = $permissionsService->getPermissionsByRole($entity);
        $rolePermissionsSelectValues = [];
        foreach ($rolePermissions as $rolePermission) {
            foreach ($rolePermission->getActions() as $action) {
                $permission = $rolePermission->getResourceId() . '::' . $action;
                $rolePermissionsSelectValues[$permission] = [
                    'label' => $permission,
                    'value' => $permission,
                ];
            }
        }
        $rolePermissionsSelect = new Select('rolePermissions', [
            'value_options' => $rolePermissionsSelectValues,
            'attributes' => [
                'multiple' => true,
            ]
        ]);
        $rolePermissionsSelect->setAttributes([
            'id' => 'allowed',
            'multiple' => true,
        ]);
        # endregion

        // 2. Create denied multi-select and filter denied guards that are allowed permissions
        # region denied
        $sortedGuardRules = $permissionsService->getSortedGuardRules($entity);
        $sortedGuardRulesSelect = [];
        foreach ($sortedGuardRules as $label => $value) {
            if (isset($rolePermissionsSelectValues[$label])) {
                continue;
            }
            $sortedGuardRulesSelect[] = [
                'label' => $label,
                'value' => $label,
            ];
        }
        $sortedGuardRulesSelect = new Select('sortedGuardRules', [
            'value_options' => $sortedGuardRulesSelect,
        ]);
        $sortedGuardRulesSelect->setAttributes([
            'id' => 'denied',
            'multiple' => true,
        ]);
        # endregion

        // 3. Get guards
        $roleGuardRules = $permissionsService->getRoleGuardRules($entity);
        $parentPermissions = $permissionsService->getPermissionsByRoleParent($entity, true);
//        $sortedGuardRules = $permissionsService->getSortedGuardRules($entity, false);

        return array_merge(
            $result,
            [
                'sortedGuardRules' => $sortedGuardRulesSelect, // denied
                'rolePermissions' => $rolePermissionsSelect, // allowed
                'roleGuardRules' => $roleGuardRules, // guards
                'hasParent' => $entity->getParent() !== null,
                'parentPermissions' => $parentPermissions, // parent guards
            ]
        );
    }
}
