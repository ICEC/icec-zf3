<?php

namespace Application\Controller;

//use Application\Common\Entity\EntityServicesTraits;
use Application\Common\Entity\EntityServicesTraits;
use Application\Common\FormElementManagerIncludeTrait;
use Application\Controller\Plugin\AuthenticationPlugin;
use Application\Form\AbstractForm;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Filter\ToInt;
use Zend\Form\Element\Hidden;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\Form\FormInterface;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\ServiceManager\ServiceManager;
use Zend\Validator\Digits;
use Zend\View\Model\ViewModel;

/**
 * Class AbstractController
 * @method bool|array|Response fileprg(FormInterface $form, $redirect = null, $redirectToUrl = false)
 * @method FlashMessenger flashMessenger()
 * @method AuthenticationPlugin AuthPlugin()
 * @package Application\Controller
 */
abstract class AbstractController extends AbstractActionController
{
    use EntityServicesTraits;
    use FormElementManagerIncludeTrait;

    #region Protected Vars
    /**
     * @var
     */
    protected $entityForm;
    protected $entityFormClass;
    protected $entityService;
    protected $entityServiceClass;

    protected $rootRoute = 'default';

    //are set @ constructor tru factory
    protected $entityManager;
    protected $accessService;
    protected $authenticationService;
    protected $serviceManager;

    //set in extended Controller class
    protected $afterPostRedirectToUrl;
    protected $entityClass;

    protected $FormElementManager;
    #endregion
    
    /**
     * AbstractController constructor.
     *
     * @param EntityManager $entityManager
     * @param AccessService $accessService
     * @param AuthenticationService $authenticationService
     * @param ServiceManager $serviceManager
     * @param FormElementManagerV3Polyfill $FormElementManager
     */
    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        ServiceManager $serviceManager,
        FormElementManagerV3Polyfill $FormElementManager
    ) {
        $this->entityManager = $entityManager;
        $this->accessService = $accessService;
        $this->authenticationService = $authenticationService;
        $this->serviceManager = $serviceManager;
        $this->FormElementManager = $FormElementManager;
    }

    /**
     * @return ViewModel
     */
    public function justHTMLAction() {
        return new ViewModel([]);
    }
    
    /**
     * @return ViewModel
     */
    public function viewAction()
    {
        $this->getEventManager()->trigger('AccesControl', $this,
            array('authenticatedUser' => $this->getAccessService()->getUser(), 'routeMatch' => $this->getEvent()->getRouteMatch()));
        $objRepository = $this->getEntityManager()->getRepository($this->getEntityClass());
        $entity = $objRepository->find($this->getEvent()->getRouteMatch()->getParam('id', 0));
        return new ViewModel([
            'entity' => $entity
        ]);
    }

    /**
     * @return ViewModel
     */
    public function listAction()
    {
        $entities = $this->getEntityManager()->getRepository($this->getEntityClass())->findAll();
        return new ViewModel([
            'entities' => $entities
        ]);
    }

    /**
     * @return array|bool|Response
     */
    public function addAction()
    {
        /** @var AbstractForm $form */
        $form = $this->getEntityForm();
        /** @var Request $request */
        $request = $this->getRequest();
        $form->setAttribute('action', $request->getRequestUri());
        $this->getEventManager()->trigger('AfterAddBindAction', $this,
            array('form' => $form, 'postData' => $request->getPost()->toArray()));
        $prg = $this->fileprg($form, $request->getRequestUri(), true);
        if ($prg instanceof Response) {
            return $prg;
        } else {
            if ($prg === false) {
                return array(
                    'entityForm' => $form,
                );
            }
        }
        $savedEntity = $this->getEntityService()->persistForm($form);
        if (!$savedEntity) {
            $this->getEventManager()->trigger('AfterFailedAddAction', $this,
                array('form' => $form));
            return array(
                'entityForm' => $form,
            );
        }
        $this->getEventManager()->trigger('afterAddPersistAction', $this,
            array('form' => $form, 'postData' => $request->getPost()->toArray()));
        return $this->redirect()->toRoute($this->getAfterPostRedirectToUrl(), [], [], true);
    }

    /**
     * @return array|bool|Response
     */
    public function editAction()
    {
        /** @var AbstractForm $form */
        $id = $this->getEvent()->getRouteMatch()->getParam('id', 0);
        $form = $this->getEntityForm();

        $form->add([
            'type' => Hidden::class,
            'name' => 'id',
            'attributes' => [
                'id' => 'id',
                'value' => $id,
            ],
            'filters' => [
                [
                    'name' => ToInt::class,
                ],
            ],
            'validators' => [
                [
                    'name' => Digits::class,
                ],
            ],
        ]);

        $objRepository = $this->getEntityManager()->getRepository($this->getEntityClass());
        $entity = $objRepository->find($id);

        $form->bind($entity);
        /** @var Request $request */
        $request = $this->getRequest();
        $this->getEventManager()->trigger('afterEditBindAction', $this,
            array('form' => $form, 'postData' => $request->getPost()->toArray()));

        $form->setAttribute('action', $request->getRequestUri());
        $prg = $this->fileprg($form, $request->getRequestUri(), true);
        if ($prg instanceof Response) {
            return $prg;
        } else {
            if ($prg === false) {
                return array(
                    'entityForm' => $form,
                );
            }
        }
        $this->getEventManager()->trigger('beforePersistEditAction', $this,
            array('form' => $form, 'postData' => $request->getPost()->toArray()));
        
        $savedEntity = $this->getEntityService()->persistForm($form);
        if (!$savedEntity) {
            $this->getEventManager()->trigger('AfterFailedEditAction', $this,
                array('form' => $form));
            return array(
                'entityForm' => $form,
            );
        }
        $this->getEventManager()->trigger('afterEditPersistAction', $this,
            array('form' => $form, 'postData' => $request->getPost()->toArray()));
        return $this->redirect()->toRoute($this->getAfterPostRedirectToUrl(), [], [], true);
    }

    /**
     * @return array|\Zend\Http\Response
     */
    public function deleteAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id', 0);
        $prg = $this->prg($this->getRequest()->getRequestUri(), true);

        if ($prg instanceof Response) {
            return $prg;
        } else {
            if ($prg === false) {
                $em = $this->getEntityManager();
                $objRepository = $em->getRepository($this->getEntityClass());
                $entity = $objRepository->find($id);

                return array(
                    'entity' => $entity,
                );
            }
        }
        $post = $prg;
        $em = $this->getEntityManager();
        $objRepository = $em->getRepository($this->getEntityClass());
        $entity = $objRepository->find($id);
        if ($this->validateDelete($post)) {
            $succes = $this->getEntityService()->delete($id);
            if (is_object($succes)) {
                if (!$succes->getDeletedAt() == null) {
                    return $this->redirect()->toRoute($this->getAfterPostRedirectToUrl());
                }
            }
        }
        return array(
            'entity' => $entity,
        );
    }

    /**
     * @param $post
     * @return bool
     */
    protected function validateDelete($post)
    {
        return is_array($post) && array_key_exists('confirm', $post) && $post['confirm'] == '1';
    }
    #region Getters & Setters
    
    /**
     * @return AccessService
     */
    public function getAccessService()
    {
        return $this->accessService;
    }
    
    
    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }
    
    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }
    
    /**
     * @param mixed $entityClass
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
    }
    
    /**
     * @return mixed
     */
    public function getAfterPostRedirectToUrl()
    {
        return $this->afterPostRedirectToUrl;
    }
    
    /**
     * @param mixed $afterPostRedirectToUrl
     */
    public function setAfterPostRedirectToUrl($afterPostRedirectToUrl)
    {
        $this->afterPostRedirectToUrl = $afterPostRedirectToUrl;
    }
    #endregion
}
