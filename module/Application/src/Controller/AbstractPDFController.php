<?php
namespace Application\Controller;

use Beheer\Entity\Algemeen\ApplicatieInstellingen;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Dompdf\Dompdf;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\Http\Response;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
/**
 * Class AbstractPDFController
 *
 * @package Application\Controller
 */
abstract class AbstractPDFController extends AbstractController
{
    /** @var Dompdf */
    protected $dompdf;
    
    /**
     * @var
     */
    protected $pdf_partial_path;
    
    /**
     * @var PhpRenderer
     */
    protected $viewRender;
    
    /**
     * WerkbonController constructor.
     *
     * @param EntityManager $entityManager
     * @param AccessService $accessService
     * @param AuthenticationService $authenticationService
     * @param ServiceManager $serviceManager
     * @param FormElementManagerV3Polyfill $FormElementManager
     * @param Dompdf $dompdf
     * @param PhpRenderer $viewRender
     */
    public function __construct(
        EntityManager $entityManager,
        AccessService $accessService,
        AuthenticationService $authenticationService,
        ServiceManager $serviceManager,
        FormElementManagerV3Polyfill $FormElementManager,
        Dompdf $dompdf,
        PhpRenderer $viewRender
    ) {
        parent::__construct($entityManager, $accessService, $authenticationService, $serviceManager, $FormElementManager);
        $this->dompdf = $dompdf;
        $this->viewRender = $viewRender;
    }
    
    /**
     * @return Response
     */
    public function pdfAction() {
        error_reporting(E_ERROR | E_PARSE);
        $entity = $this->getEntityManager()->getRepository($this->getEntityClass())
            ->findOneBy(['id' => $this->getEvent()->getRouteMatch()->getParam('id', 0)]);
        $objRepository = $this->getEntityManager()->getRepository(ApplicatieInstellingen::class);
        $applicatieInstellingen = $objRepository->find(1);
        
        $viewModel = new ViewModel([
            'entity' => $entity,
            'applicatieInstellingen' => $applicatieInstellingen,
        ]);
        $viewModel->setTemplate($this->getPdfPartialPath());
        $this->getEventManager()->trigger('ViewModelBeforeRender', $this,
            array('viewModel' => $viewModel));
//        return $viewModel;
        $pdfHtml = $this->getViewRender()->render($viewModel);
        
        $this->getDompdf()->loadHtml($pdfHtml);
        $this->getDompdf()->render();
        
        /** @var Response $response */
        $response = $this->getResponse();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/pdf');
        $headers->addHeaderLine('Content-Disposition', "inline; filename=\"document.pdf\"");
        
        $response->setContent($this->getDompdf()->output());
        return $response;
    }
    
    #region Getters & Setters
    /**
     * @return PhpRenderer
     */
    public function getViewRender(): PhpRenderer {
        return $this->viewRender;
    }
    
    /**
     * @return Dompdf
     */
    public function getDompdf(): Dompdf {
        return $this->dompdf;
    }
    
    /**
     * @return mixed
     */
    public function getPdfPartialPath() {
        return $this->pdf_partial_path;
    }
    
    /**
     * @param mixed $pdf_partial_path
     */
    public function setPdfPartialPath($pdf_partial_path) {
        $this->pdf_partial_path = $pdf_partial_path;
    }
    #endregion
}
