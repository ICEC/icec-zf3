<?php
namespace Application\Controller\Plugin;

use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class AuthenticationPlugin
 *
 * @package Application\Controller\Plugin
 */
class AuthenticationPlugin extends \CirclicalUser\Controller\Plugin\AuthenticationPlugin {
    
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;
    
    /**
     * @var AccessService
     */
    protected $accessService;
    
    /**
     * AuthenticationPlugin constructor.
     *
     * @param AuthenticationService $authenticationService
     * @param AccessService $accessService
     */
    public function __construct(AuthenticationService $authenticationService, AccessService $accessService) {
        $this->authenticationService = $authenticationService;
        $this->accessService = $accessService;
    }
    
    /**
     * @return AccessService
     */
    public function getAccessService() {
        return $this->accessService;
    }
    
    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService() {
        return $this->authenticationService;
    }    
}