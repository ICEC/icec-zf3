<?php
namespace Application\Controller;

use Application\Entity\Medewerker;
use Application\Entity\User;
use Application\Form\MedewerkerForm;
use CirclicalUser\Entity\Authentication;
use Zend\View\Model\ViewModel;

/**
 * Class MedewerkerController
 *
 * @package Dashboard\Controller
 */
class MedewerkerController extends AbstractController {
    
    protected $afterPostRedirectToUrl = 'medewerker';
    protected $entityClass = Medewerker::class;
    protected $entityServiceClass = \Application\Service\Medewerker::class;
    protected $entityFormClass = MedewerkerForm::class;


    /**
     * @return ViewModel
     */
    public function listAction() {
        /** @var User $user */
        foreach ($this->getEntityManager()->getRepository($this->getEntityClass())->findAll() as $user) {
            if (is_object($this->getEntityManager()->getRepository(Authentication::class)->findOneBy(['username' => $user->getEmail()]))) {
                $user->setHasAuthCreated(true);
            }
        }
        return new ViewModel([
            'entities' => $this->getEntityManager()->getRepository($this->getEntityClass())->findAll()
        ]);
    }

}
