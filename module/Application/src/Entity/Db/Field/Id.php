<?php
namespace Application\Entity\Db\Field;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Class Id
 *
 * @package Application\src\Entity\Db\Field
 */
trait Id {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Form\Exclude()
     */
    protected $id;
    
    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return AbstractEntity
     */
    public function setId($id) {
        $this->id = (int)$id;
        return $this;
    }
}
