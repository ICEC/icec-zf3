<?php
namespace Application\Entity\Db\Field;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Class Created
 *
 * @package Application\src\Entity\Db\Field
 */
trait Created {
    /**
     * @ORM\Column(type="datetime")
     * @Form\Exclude()
     * @var \DateTime
     */
    protected $created = null;
    
    /**
     * Getter for $created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }
    
    /**
     * @param \DateTime $created
     * @return AbstractEntity
     */
    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }
}
