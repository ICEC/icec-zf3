<?php
namespace Application\Entity\Db\Field;

use Application\Entity\AbstractEntity;

/**
 * Class Deleted
 *
 * @package Application\src\Entity\Db\Field
 */
trait Deleted {
    /**
     * Deleted At datetime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Form\Exclude()
     * @var \DateTime
     */
    protected $deleted_at;
    
    /**
     * @return \DateTime
     */
    public function getDeletedAt() {
        return $this->deleted_at;
    }
        
    /**
     * @param $deleted_at
     * @return AbstractEntity
     */
    public function setDeletedAt($deleted_at) {
        $this->deleted_at = $deleted_at;
        return $this;
    }
}