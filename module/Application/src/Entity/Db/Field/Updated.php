<?php
namespace Application\Entity\Db\Field;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Class Updated
 *
 * @package Application\src\Entity\Db\Field
 */
trait Updated {
    /**
     * @ORM\Column(type="datetime")
     * @Form\Exclude()
     * @var \DateTime
     */
    protected $updated_at = null;
    
    /**
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    
    /**
     * @param \DateTime $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        return $this;
    }

}
