<?php
namespace Application\Entity;

/**
 * Interface BaseEntityInterface
 *
 * @package Application\Entity
 */
interface BaseEntityInterface {
    /**
     * @return mixed
     */
    public function getId();
    
    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);
    
    /**
     * @return mixed
     */
    public function getCreated();
    
    /**
     * @param $created
     * @return mixed
     */
    public function setCreated($created);
    
    /**
     * @return mixed
     */
    public function getUpdatedAt();
    
    /**
     * @param $updated
     * @return mixed
     */
    public function setUpdatedAt($updated);
    
}
