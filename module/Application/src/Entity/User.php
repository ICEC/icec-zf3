<?php
namespace Application\Entity;

use Application\Common\Provider\FullUsernameInterface;
use CirclicalUser\Provider\RoleInterface;
use CirclicalUser\Provider\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;
/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\InheritanceType("JOINED")
 *
 * @ORM\Entity(repositoryClass="Application\Repository\UserRepository")
 * @Form\Name("formUser")
 * @Form\Hydrator("Zend\Hydrator\ObjectProperty")
 * @Form\Type("fieldset")
 */
abstract class User extends AbstractEntity implements UserInterface, FullUsernameInterface {
    
    /**
     * Initialies the roles variable.
     */
    public function __construct() {
        parent::__construct();
        $this->roles = new ArrayCollection();
    }
    
    /**
     * @var string
     * @ORM\Column(type="string", unique=true,  length=255)
     * @Form\Type("Zend\Form\Element\Email")
     * @Form\Required({"required":"true" })
     * @Form\Filter({"name":"StripTags"})
     * @Form\Attributes({
     *     "placeholder":"Email adres van medewerker",
     * })
     * @Form\Options({
     *     "label":"Email",
     *     "class":"text-right middle",
     *     "label_options":{"class":"text-right middle"},
     * })
     *
     *
     */
    protected $email;
        
    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="CirclicalUser\Entity\Role")
     * @ORM\JoinTable(name="users_roles",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     * @Form\Exclude()
     */
    protected $roles;


    protected $has_auth_created = false;

    /**
     * @return mixed
     */
    public function getHasAuthCreated() {
        return $this->has_auth_created;
    }

    /**
     * @param mixed $has_auth_created
     */
    public function setHasAuthCreated($has_auth_created) {
        $this->has_auth_created = $has_auth_created;
    }
    
    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
    
    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }
    
    
    /**
     * @return ArrayCollection
     */
    public function getRoles() {
        return $this->roles;
    }
        
    /**
     * @param RoleInterface $role
     */
    public function addRole(RoleInterface $role) {
        $this->roles[] = $role;
    }
    
    /**
     * @param ArrayCollection $roles
     * @return $this
     */
    public function removeRoles(ArrayCollection $roles) {
        foreach ($roles as $role) {
            $this->roles->removeElement($role);
        }
        return $this;
    }
    
    /**
     * @param ArrayCollection $roles
     * @return $this
     */
    public function addRoles(ArrayCollection $roles) {
        foreach ($roles as $role) {
            $this->roles->add($role);
        }
        return $this;
    }
    
    /**
     * Returns the full username of a UserEntity
     * @return string
     */
    public function getUserFullName() {
        return (string) $this->getVoornaam().' '. $this->getAchternaam();
    }
}