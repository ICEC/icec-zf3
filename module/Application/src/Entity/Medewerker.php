<?

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="medewerker")
 * @ORM\Entity(repositoryClass="Application\Repository\MedewerkerRepository")
 *
 * @Form\Name("formMedewerker")
 * @Form\Hydrator("Zend\Hydrator\ObjectProperty")
 * @Form\Type("Application\Form\AbstractForm")
 */
class Medewerker extends User {
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $voornaam;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $achternaam;
    
    #region Getters & Setters
    /**
     * @return string
     */
    public function getVoornaam() {
        return $this->voornaam;
    }
    
    /**
     * @param string $voornaam
     */
    public function setVoornaam($voornaam) {
        $this->voornaam = $voornaam;
    }
    
    /**
     * @return string
     */
    public function getAchternaam() {
        return $this->achternaam;
    }
    
    /**
     * @param string $achternaam
     */
    public function setAchternaam($achternaam) {
        $this->achternaam = $achternaam;
    }
    #endregion
}