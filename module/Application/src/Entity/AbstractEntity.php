<?php
namespace Application\Entity;

use Application\Entity\Db\Field\Created;
use Application\Entity\Db\Field\Deleted;
use Application\Entity\Db\Field\Id;

use Application\Entity\Db\Field\Updated;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Class AbstractEntity
 *
 * @package Application\Entity
 *
 * @ORM\MappedSuperclass
 * @Form\Name("entity") Not necessary, but there must be at least one line with Form to use the "use" statement without complains from IDE
 *     and cs-fixer
 */
abstract class AbstractEntity implements BaseEntityInterface {
    use Id, Created, Updated, Deleted;
    
    /**
     * AbstractEntity constructor.
     */
    public function __construct() {
        $this->created = new \DateTime();
        $this->updated_at = new \DateTime();
    }
}