<?php

namespace Application\Common;

/**
 * Trait ObjectManagerIncludeTrait
 *
 * @package Application\Common
 */
trait ObjectManagerIncludeTrait {
    /**
     * @var
     */
    protected $objectManager;
    
    /**
     * @return mixed
     */
    public function getObjectManager() {
        return $this->objectManager;
    }
    
    /**
     * @param mixed $objectManager
     */
    public function setObjectManager($objectManager) {
        $this->objectManager = $objectManager;
    }
    
}