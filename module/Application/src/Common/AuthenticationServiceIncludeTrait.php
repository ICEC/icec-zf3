<?php
namespace Application\Common;
/**
 * Trait AuthenticationServiceIncludeTrait
 *
 * @package Application\Common
 */
trait AuthenticationServiceIncludeTrait {
    /**
     * @var
     */
    protected $authenticationService;
    
    /**
     * @return mixed
     */
    public function getAuthenticationService() {
        return $this->authenticationService;
    }
    
    /**
     * @param mixed $authenticationService
     */
    public function setAuthenticationService($authenticationService) {
        $this->authenticationService = $authenticationService;
    }
}