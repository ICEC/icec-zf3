<?php
namespace Application\Common;

use Zend\Form\Element\Collection;
use Zend\Form\FieldsetInterface;
use 
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

/**
 * Class FieldsetHydratorTrait
 *
 * @package Application\Common
 */
trait FieldsetHydratorTrait {
    
    /**
     * @param FieldsetInterface $form
     * @param $entityClass
     * @param null $entityManager
     */
    protected function setFieldSetHydrator(FieldsetInterface $form, $entityClass, $entityManager = null) {
        $entityManager = !is_null($entityManager) ? $entityManager : $this->getEntityManager();
        //TODO fix embed this in function below check TargetElement
        if($form instanceof Collection){
            $form->getTargetElement()->setHydrator(new DoctrineHydrator($entityManager, $entityClass));
        }else{
            $form->setHydrator(new DoctrineHydrator($entityManager, $entityClass));
        }
        
        // check nested FieldSets
        /** @var FieldsetInterface $fieldset */
        foreach ($form->getFieldsets() as $fieldset) {
            if (array_key_exists('target_element', $fieldset->getOptions())) {
                $targetClass = $fieldset->getOptions()['target_element'];
                if (array_key_exists('type', $targetClass))
                    $targetClass = $targetClass['type'];
                $this->setFieldSetHydrator($fieldset, $targetClass, $entityManager);
            }
        }
        
        // check TargetElement
        if (method_exists($form, 'getTargetElement')) {
            $targetElement = $form->getTargetElement();
            $targetClass = get_class($targetElement->getObject());
            if (!$targetClass && array_key_exists('target_element', $targetElement->getOptions())) {
                $targetClass = $targetElement->getOptions()['target_element'];
                if (array_key_exists('type', $targetClass))
                    $targetClass = $targetClass['type'];
            }
            $this->setFieldSetHydrator($targetElement, $targetClass, $entityManager);
        }                
    }    
}