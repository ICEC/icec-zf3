<?

namespace Application\Common;

use DateTime;

/**
 * Trait TimeDurationTrait
 *
 * @package Application\Common
 */
trait TimeDurationTrait {
    
    /**
     * @param DateTime $van
     * @param DateTime $tot
     * @return string
     */
    public static function getDurationBetween(DateTime $van, DateTime $tot) {
        /** @var DateTime $van */
        /** @var DateTime $tot */
        $duration = $van->diff($tot);
        if ($duration->h > 0) {
            $show_hour = $duration->h . ':';
        } else {
            $show_hour = '00:';
        }
        if ($duration->i > 0) {
            $show_minuts = $duration->i;
        } else {
            $show_minuts = '00';
        }
        return $show_hour . '' . $show_minuts;
    }
}