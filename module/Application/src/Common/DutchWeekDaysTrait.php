<?
namespace Application\Common;

/**
 * Trait DutchWeekDaysTrait
 *
 * @package module\Application\src\Common
 */
trait DutchWeekDaysTrait {
    
    public static function getWeekDayLabel($week_day) {
    
        switch ($week_day) {
            case 0:
                $day_label = 'Zondag';
                break;
            case 1:
                $day_label = 'Maandag';
                break;
            case 2:
                $day_label = 'Dinsdag';
                break;
            case 3:
                $day_label = 'Woensdag';
                break;
            case 4:
                $day_label = 'Donderdag';
                break;
            case 5:
                $day_label = 'Vrijdag';
                break;
            case 6:
                $day_label = 'Zaterdag';
                break;

            default:
                $day_label = '';
                break;
        }
        return $day_label;
    }
}