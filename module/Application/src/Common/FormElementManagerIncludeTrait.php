<?php
namespace Application\Common;

/**
 * Trait FormElementManagerIncludeTrait
 *
 * @package Application\Common
 */
trait FormElementManagerIncludeTrait {
    /**
     * @var
     */
    protected $FormElementManager;
    
    /**
     * @return mixed
     */
    public function getFormElementManager() {
        return $this->FormElementManager;
    }
    
    /**
     * @param mixed $FormElementManager
     */
    public function setFormElementManager($FormElementManager) {
        $this->FormElementManager = $FormElementManager;
    }
}