<?php
namespace Application\Common;

use Zend\Form\Fieldset;
/**
 * Class IdFieldsetTrait
 *
 * @package Application\Common
 */
trait IdFieldsetTrait {
    
    /**
     * @param Fieldset $formOrFieldset
     */
    public function addHiddenIdElement(Fieldset $formOrFieldset) {
        $formOrFieldset->add([
            'type'  => 'hidden',
            'name' => 'id',
            'attributes' => [
                'required' => false,
            ],
        ]); 
    }
}