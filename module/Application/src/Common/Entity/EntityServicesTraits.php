<?php
namespace Application\Common\Entity;

use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;

/**
 * Class EntityServicesTraits
 *
 * @package Application\Common\Entity
 */
trait EntityServicesTraits {
    
    /**
     * @return mixed
     */
    public function getEntityForm() {
        if (null === $this->entityForm) {
            $entityFormFieldsetClass = $this->getEntityFormClass();
            if (!class_exists($entityFormFieldsetClass)) {
                throw new \RuntimeException("Form / Fieldset $entityFormFieldsetClass doesn't exist!");
            }
            $this->entityForm = $this->getFormElementManager()->get($entityFormFieldsetClass);
        }
        return $this->entityForm;
    }
    
    /**
     * @return mixed
     */
    public function getEntityService() {
        if (null === $this->entityService) {
            $entityServiceClass = $this->getEntityServiceClass();
            if (!class_exists($entityServiceClass)) {
                throw new \RuntimeException("Class $entityServiceClass doesn't exist!");
            }
            $this->entityService = new $entityServiceClass(
                $this->getEntityManager(),
                $this->getAuthenticationService(),
                $this->getFormElementManager()
                );
        }
        return $this->entityService;
    }
    
    #region Getters & Setters
    /**
     * @return mixed
     */
    public function getEntityFormClass() {
        return $this->entityFormClass;
    }
    
    /**
     * @return mixed
     */
    public function getEntityServiceClass() {
        return $this->entityServiceClass;
    }

    /**
     * @param mixed $entityFormClass
     */
    public function setEntityFormClass($entityFormClass) {
        $this->entityFormClass = $entityFormClass;
    }


    /**
     * @param mixed $entityServiceClass
     */
    public function setEntityServiceClass($entityServiceClass) {
        $this->entityServiceClass = $entityServiceClass;
    }
    
    /**
     * @return EntityManager
     */
    public function getEntityManager() {
        return $this->entityManager;
    }
    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService() {
        return $this->authenticationService;
    }
    
    
    #endregion
}