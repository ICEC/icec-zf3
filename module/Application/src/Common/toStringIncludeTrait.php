<?
namespace Application\Common;
/**
 * Trait toStringIncludeTrait
 *
 * @package Application\Common
 */
trait toStringIncludeTrait {
    
    /**
     *  Returns the Id of an object
     *      mostly used by ObjectSelect FormHelper
     * @return string
     */
    function __toString() {
        return strtolower($this->getId());
    }
}