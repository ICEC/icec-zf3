<?
namespace Application\Common;

/**
 * Trait CountryListAsOptions
 *
 * @package Application\Common
 */
trait CountryListAsOptions {
    
    public static function getLandLabelWithNumber($value) {
        //if Nederland ? forget the list
        if ($value == 528):
            return 'Nederland';
        endif;
    
        #region Gugly for now
        $county_list = [
            ["Afghanistan", 4],
            ["Albanië", 8],
            ["Algerije", 12],
            ["Amerikaans-Samoa", 16],
            ["Amerikaanse Maagdeneilanden", 850],
            ["Andorra", 20],
            ["Angola", 24],
            ["Anguilla", 660],
            ["Antarctica", 10],
            ["Antigua en Barbuda", 28],
            ["Argentinië", 32],
            ["Armenië", 51],
            ["Aruba", 533],
            ["Australië", 36],
            ["Azerbeidzjan", 31],
            ["Bahama's", 44],
            ["Bahrein", 48],
            ["Bangladesh", 50],
            ["Barbados", 52],
            ["België", 56],
            ["Belize", 84],
            ["Benin", 204],
            ["Bermuda", 60],
            ["Bhutan", 64],
            ["Bolivia", 68],
            ["Bosnië en Herzegovina", 70],
            ["Botswana", 72],
            ["Bouveteiland", 74],
            ["Brazilië", 76],
            ["Brits Indische Oceaanterritorium", 86],
            ["Britse Maagdeneilanden", 92],
            ["Brunei", 96],
            ["Bulgarije", 100],
            ["Burkina Faso", 854],
            ["Burundi", 108],
            ["Cambodja", 116],
            ["Canada", 124],
            ["Centraal-Afrikaanse Republiek", 140],
            ["Chili", 152],
            ["China", 156],
            ["Christmaseiland", 162],
            ["Cocoseilanden", 166],
            ["Colombia", 170],
            ["Comoren", 174],
            ["Congo-Brazzaville", 178],
            ["Congo-Kinshasa", 180],
            ["Cookeilanden", 184],
            ["Costa Rica", 188],
            ["Cuba", 192],
            ["Curaçao", 531],
            ["Cyprus", 196],
            ["Denemarken", 208],
            ["Djibouti", 262],
            ["Dominicaanse Republiek", 214],
            ["Dominica", 212],
            ["Duitsland", 276],
            ["Ecuador", 218],
            ["Egypte", 818],
            ["El Salvador", 222],
            ["Equatoriaal-Guinea", 226],
            ["Eritrea", 232],
            ["Estland", 233],
            ["Ethiopië", 231],
            ["Faeröer", 234],
            ["Falklandeilanden", 238],
            ["Fiji", 242],
            ["Filipijnen", 608],
            ["Finland", 246],
            ["Frankrijk", 250],
            ["Frans-Guyana", 254],
            ["Frans-Polynesië", 258],
            ["Franse Zuidelijke en Antarctische Gebieden", 260],
            ["Gabon", 266],
            ["Gambia", 270],
            ["Georgië", 268],
            ["Ghana", 288],
            ["Gibraltar", 292],
            ["Grenada", 308],
            ["Griekenland", 300],
            ["Groenland", 304],
            ["Guadeloupe", 312],
            ["Guam", 316],
            ["Guatemala", 320],
            ["Guernsey", 831],
            ["Guinee-Bissau", 624],
            ["Guinee", 324],
            ["Guyana", 328],
            ["Haïti", 332],
            ["Heard en McDonaldeilanden", 334],
            ["Honduras", 340],
            ["Hongarije", 348],
            ["Hongkong", 344],
            ["Ierland", 372],
            ["IJsland", 352],
            ["India", 356],
            ["Indonesië", 360],
            ["Irak", 368],
            ["Iran", 364],
            ["Israël", 376],
            ["Italië", 380],
            ["Ivoorkust", 384],
            ["Jamaica", 388],
            ["Japan", 392],
            ["Jemen", 887],
            ["Jersey", 832],
            ["Jordanië", 400],
            ["Kaaimaneilanden", 136],
            ["Kaapverdië", 132],
            ["Kameroen", 120],
            ["Kazachstan", 398],
            ["Kenia", 404],
            ["Kirgizië", 417],
            ["Kiribati", 296],
            ["Kleine afgelegen eilanden van de Verenigde Staten", 581],
            ["Koeweit", 414],
            ["Kroatië", 191],
            ["Laos", 418],
            ["Lesotho", 426],
            ["Letland", 428],
            ["Libanon", 422],
            ["Liberia", 430],
            ["Libië", 434],
            ["Liechtenstein", 438],
            ["Litouwen", 440],
            ["Luxemburg", 442],
            ["Macau", 446],
            ["Macedonië", 807],
            ["Madagaskar", 450],
            ["Malawi", 454],
            ["Maldiven", 462],
            ["Maleisië", 458],
            ["Mali", 466],
            ["Malta", 470],
            ["Man", 833],
            ["Marokko", 504],
            ["Marshalleilanden", 584],
            ["Martinique", 474],
            ["Mauritanië", 478],
            ["Mauritius", 480],
            ["Mayotte", 175],
            ["Mexico", 484],
            ["Micronesia", 583],
            ["Moldavië", 498],
            ["Monaco", 492],
            ["Mongolië", 496],
            ["Montenegro", 499],
            ["Montserrat", 500],
            ["Mozambique", 508],
            ["Myanmar", 104],
            ["Namibië", 516],
            ["Nauru", 520],
            ["Caribisch Nederland", 535],
            ["Nederland", 528],
            ["Nepal", 524],
            ["Nicaragua", 558],
            ["Nieuw-Caledonië", 540],
            ["Nieuw-Zeeland", 554],
            ["Nigeria", 566],
            ["Niger", 562],
            ["Niue", 570],
            ["Noord-Korea", 408],
            ["Noordelijke Marianen", 580],
            ["Noorwegen", 578],
            ["Norfolk", 574],
            ["Oeganda", 800],
            ["Oekraïne", 804],
            ["Oezbekistan", 860],
            ["Oman", 512],
            ["Oost-Timor", 626],
            ["Oostenrijk", 40],
            ["Pakistan", 586],
            ["Palau", 585],
            ["Palestina", 275],
            ["Panama", 591],
            ["Papoea-Nieuw-Guinea", 598],
            ["Paraguay", 600],
            ["Peru", 604],
            ["Pitcairneilanden", 612],
            ["Polen", 616],
            ["Portugal", 620],
            ["Puerto Rico", 630],
            ["Qatar", 634],
            ["Roemenië", 642],
            ["Rusland", 643],
            ["Rwanda", 646],
            ["Réunion", 638],
            ["Saint Kitts en Nevis", 659],
            ["Saint Lucia", 662],
            ["Saint Vincent en de Grenadines", 670],
            ["Saint-Barthélemy", 652],
            ["Saint-Pierre en Miquelon", 666],
            ["Salomonseilanden", 90],
            ["Samoa", 882],
            ["San Marino", 674],
            ["Sao Tomé en Principe", 678],
            ["Saoedi-Arabië", 682],
            ["Senegal", 686],
            ["Servië", 688],
            ["Seychellen", 690],
            ["Sierra Leone", 694],
            ["Singapore", 702],
            ["Sint Maarten", 534],
            ["Sint-Maarten", 663],
            ["Slovenië", 705],
            ["Slowakije", 703],
            ["Soedan", 729],
            ["Somalië", 706],
            ["Spanje", 724],
            ["Spitsbergen en Jan Mayen", 744],
            ["Sri Lanka", 144],
            ["Suriname", 740],
            ["Swaziland", 748],
            ["Syrië", 760],
            ["Tadzjikistan", 762],
            ["Taiwan", 158],
            ["Tanzania", 834],
            ["Thailand", 764],
            ["Togo", 768],
            ["Tokelau", 772],
            ["Tonga", 776],
            ["Trinidad en Tobago", 780],
            ["Tsjaad", 148],
            ["Tsjechië", 203],
            ["Tunesië", 788],
            ["Turkije", 792],
            ["Turkmenistan", 795],
            ["Turks- en Caicoseilanden", 796],
            ["Tuvalu", 798],
            ["Uruguay", 858],
            ["Vanuatu", 548],
            ["Vaticaanstad", 336],
            ["Venezuela", 862],
            ["Sint-Helena, Ascension en Tristan da Cunha", 654],
            ["Verenigd Koninkrijk", 826],
            ["Verenigde Arabische Emiraten", 784],
            ["Verenigde Staten", 840],
            ["Vietnam", 704],
            ["Wallis en Futuna", 876],
            ["Westelijke Sahara", 732],
            ["Wit-Rusland", 112],
            ["Zambia", 894],
            ["Zimbabwe", 716],
            ["Zuid-Afrika", 710],
            ["Zuid-Georgia en de Zuidelijke Sandwicheilanden", 239],
            ["Zuid-Korea", 410],
            ["Zuid-Soedan", 728],
            ["Zweden", 752],
            ["Zwitserland", 756],
            ["Åland", 248],
        ];
        #endregion
        foreach ($county_list as $key => $option):
            if ($option[1] == $value):
                return $option[0];
            endif;
        endforeach;
        return '';
    }
    
    /**
     * @param $value
     * @return string
     */
    public function getCountryLabelByValue($value) {
        //if Nederland ? forget the list
        if ($value == 528):
            return 'Nederland';
        endif;
        
        foreach ($this->county_list as $key => $option):
            if ($option[1] == $value):
                return $option[0];
            endif;
        endforeach;
        return '';
    }
    
    /**
     * @return mixed
     */
    public function getCountryOptions() {
        $county_options = [];
        foreach ($this->county_list as $key => $option):
            $county_options[$key] = [
                'value' => $option[1],
                'label' => $option[0],
            ];
        endforeach;
        return $county_options;
    }
    
    /**
     * @var array
     */
    protected $county_list = [
        ["Afghanistan", 4],
        ["Albanië", 8],
        ["Algerije", 12],
        ["Amerikaans-Samoa", 16],
        ["Amerikaanse Maagdeneilanden", 850],
        ["Andorra", 20],
        ["Angola", 24],
        ["Anguilla", 660],
        ["Antarctica", 10],
        ["Antigua en Barbuda", 28],
        ["Argentinië", 32],
        ["Armenië", 51],
        ["Aruba", 533],
        ["Australië", 36],
        ["Azerbeidzjan", 31],
        ["Bahama's", 44],
        ["Bahrein", 48],
        ["Bangladesh", 50],
        ["Barbados", 52],
        ["België", 56],
        ["Belize", 84],
        ["Benin", 204],
        ["Bermuda", 60],
        ["Bhutan", 64],
        ["Bolivia", 68],
        ["Bosnië en Herzegovina", 70],
        ["Botswana", 72],
        ["Bouveteiland", 74],
        ["Brazilië", 76],
        ["Brits Indische Oceaanterritorium", 86],
        ["Britse Maagdeneilanden", 92],
        ["Brunei", 96],
        ["Bulgarije", 100],
        ["Burkina Faso", 854],
        ["Burundi", 108],
        ["Cambodja", 116],
        ["Canada", 124],
        ["Centraal-Afrikaanse Republiek", 140],
        ["Chili", 152],
        ["China", 156],
        ["Christmaseiland", 162],
        ["Cocoseilanden", 166],
        ["Colombia", 170],
        ["Comoren", 174],
        ["Congo-Brazzaville", 178],
        ["Congo-Kinshasa", 180],
        ["Cookeilanden", 184],
        ["Costa Rica", 188],
        ["Cuba", 192],
        ["Curaçao", 531],
        ["Cyprus", 196],
        ["Denemarken", 208],
        ["Djibouti", 262],
        ["Dominicaanse Republiek", 214],
        ["Dominica", 212],
        ["Duitsland", 276],
        ["Ecuador", 218],
        ["Egypte", 818],
        ["El Salvador", 222],
        ["Equatoriaal-Guinea", 226],
        ["Eritrea", 232],
        ["Estland", 233],
        ["Ethiopië", 231],
        ["Faeröer", 234],
        ["Falklandeilanden", 238],
        ["Fiji", 242],
        ["Filipijnen", 608],
        ["Finland", 246],
        ["Frankrijk", 250],
        ["Frans-Guyana", 254],
        ["Frans-Polynesië", 258],
        ["Franse Zuidelijke en Antarctische Gebieden", 260],
        ["Gabon", 266],
        ["Gambia", 270],
        ["Georgië", 268],
        ["Ghana", 288],
        ["Gibraltar", 292],
        ["Grenada", 308],
        ["Griekenland", 300],
        ["Groenland", 304],
        ["Guadeloupe", 312],
        ["Guam", 316],
        ["Guatemala", 320],
        ["Guernsey", 831],
        ["Guinee-Bissau", 624],
        ["Guinee", 324],
        ["Guyana", 328],
        ["Haïti", 332],
        ["Heard en McDonaldeilanden", 334],
        ["Honduras", 340],
        ["Hongarije", 348],
        ["Hongkong", 344],
        ["Ierland", 372],
        ["IJsland", 352],
        ["India", 356],
        ["Indonesië", 360],
        ["Irak", 368],
        ["Iran", 364],
        ["Israël", 376],
        ["Italië", 380],
        ["Ivoorkust", 384],
        ["Jamaica", 388],
        ["Japan", 392],
        ["Jemen", 887],
        ["Jersey", 832],
        ["Jordanië", 400],
        ["Kaaimaneilanden", 136],
        ["Kaapverdië", 132],
        ["Kameroen", 120],
        ["Kazachstan", 398],
        ["Kenia", 404],
        ["Kirgizië", 417],
        ["Kiribati", 296],
        ["Kleine afgelegen eilanden van de Verenigde Staten", 581],
        ["Koeweit", 414],
        ["Kroatië", 191],
        ["Laos", 418],
        ["Lesotho", 426],
        ["Letland", 428],
        ["Libanon", 422],
        ["Liberia", 430],
        ["Libië", 434],
        ["Liechtenstein", 438],
        ["Litouwen", 440],
        ["Luxemburg", 442],
        ["Macau", 446],
        ["Macedonië", 807],
        ["Madagaskar", 450],
        ["Malawi", 454],
        ["Maldiven", 462],
        ["Maleisië", 458],
        ["Mali", 466],
        ["Malta", 470],
        ["Man", 833],
        ["Marokko", 504],
        ["Marshalleilanden", 584],
        ["Martinique", 474],
        ["Mauritanië", 478],
        ["Mauritius", 480],
        ["Mayotte", 175],
        ["Mexico", 484],
        ["Micronesia", 583],
        ["Moldavië", 498],
        ["Monaco", 492],
        ["Mongolië", 496],
        ["Montenegro", 499],
        ["Montserrat", 500],
        ["Mozambique", 508],
        ["Myanmar", 104],
        ["Namibië", 516],
        ["Nauru", 520],
        ["Caribisch Nederland", 535],
        ["Nederland", 528],
        ["Nepal", 524],
        ["Nicaragua", 558],
        ["Nieuw-Caledonië", 540],
        ["Nieuw-Zeeland", 554],
        ["Nigeria", 566],
        ["Niger", 562],
        ["Niue", 570],
        ["Noord-Korea", 408],
        ["Noordelijke Marianen", 580],
        ["Noorwegen", 578],
        ["Norfolk", 574],
        ["Oeganda", 800],
        ["Oekraïne", 804],
        ["Oezbekistan", 860],
        ["Oman", 512],
        ["Oost-Timor", 626],
        ["Oostenrijk", 40],
        ["Pakistan", 586],
        ["Palau", 585],
        ["Palestina", 275],
        ["Panama", 591],
        ["Papoea-Nieuw-Guinea", 598],
        ["Paraguay", 600],
        ["Peru", 604],
        ["Pitcairneilanden", 612],
        ["Polen", 616],
        ["Portugal", 620],
        ["Puerto Rico", 630],
        ["Qatar", 634],
        ["Roemenië", 642],
        ["Rusland", 643],
        ["Rwanda", 646],
        ["Réunion", 638],
        ["Saint Kitts en Nevis", 659],
        ["Saint Lucia", 662],
        ["Saint Vincent en de Grenadines", 670],
        ["Saint-Barthélemy", 652],
        ["Saint-Pierre en Miquelon", 666],
        ["Salomonseilanden", 90],
        ["Samoa", 882],
        ["San Marino", 674],
        ["Sao Tomé en Principe", 678],
        ["Saoedi-Arabië", 682],
        ["Senegal", 686],
        ["Servië", 688],
        ["Seychellen", 690],
        ["Sierra Leone", 694],
        ["Singapore", 702],
        ["Sint Maarten", 534],
        ["Sint-Maarten", 663],
        ["Slovenië", 705],
        ["Slowakije", 703],
        ["Soedan", 729],
        ["Somalië", 706],
        ["Spanje", 724],
        ["Spitsbergen en Jan Mayen", 744],
        ["Sri Lanka", 144],
        ["Suriname", 740],
        ["Swaziland", 748],
        ["Syrië", 760],
        ["Tadzjikistan", 762],
        ["Taiwan", 158],
        ["Tanzania", 834],
        ["Thailand", 764],
        ["Togo", 768],
        ["Tokelau", 772],
        ["Tonga", 776],
        ["Trinidad en Tobago", 780],
        ["Tsjaad", 148],
        ["Tsjechië", 203],
        ["Tunesië", 788],
        ["Turkije", 792],
        ["Turkmenistan", 795],
        ["Turks- en Caicoseilanden", 796],
        ["Tuvalu", 798],
        ["Uruguay", 858],
        ["Vanuatu", 548],
        ["Vaticaanstad", 336],
        ["Venezuela", 862],
        ["Sint-Helena, Ascension en Tristan da Cunha", 654],
        ["Verenigd Koninkrijk", 826],
        ["Verenigde Arabische Emiraten", 784],
        ["Verenigde Staten", 840],
        ["Vietnam", 704],
        ["Wallis en Futuna", 876],
        ["Westelijke Sahara", 732],
        ["Wit-Rusland", 112],
        ["Zambia", 894],
        ["Zimbabwe", 716],
        ["Zuid-Afrika", 710],
        ["Zuid-Georgia en de Zuidelijke Sandwicheilanden", 239],
        ["Zuid-Korea", 410],
        ["Zuid-Soedan", 728],
        ["Zweden", 752],
        ["Zwitserland", 756],
        ["Åland", 248],
    ];
}