<?php
namespace Application\Common;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * Class BaseModalFormElements
 *
 * @package Application\Common
 */
trait BaseModalFormElements {
    
    /**
     * @param Fieldset $formOrFieldset
     */
    public function addBaseModalFormElement(Fieldset $formOrFieldset) {
    
        $formOrFieldset->add([
            'type'  => 'hidden',
            'name' => 'action',
            'attributes' => [
                'required' => false,
            ],
        ]);
        $formOrFieldset->add([
            'type'  => 'hidden',
            'name' => 'succes',
            'attributes' => [
                'required' => false,
            ],
        ]);
        $formOrFieldset->add([
            'type'  => 'hidden',
            'name' => 'id',
            'attributes' => [
                'required' => false,
            ],
        ]);
        
        $formOrFieldset->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);        
    }
    
    public function addBaseModalFormElementInputFilters(InputFilter $inputFilter)
    {
       
        $inputFilter->add([
                'name'     => 'action',
                'required' => false,
            ]
        );
        $inputFilter->add([
                'name'     => 'succes',
                'required' => false,
            ]
        );
        
        $inputFilter->add([
                'name'     => 'id',
                'required' => false,
            ]
        );
    }
}