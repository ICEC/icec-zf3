<?php
namespace Application\Common;

use Zend\Form\FieldsetInterface;
/**
 * Class ObjectManagerForElementsTrait
 *
 * @package Application\Common
 */
trait ObjectManagerForElementsTrait {
    
    /**
     * @return mixed
     */
    public function getEntityManager() {
        return $this->entityManager;
    }
    
    /**
     * @param FieldsetInterface $form
     * @param null $entityManager
     */
    protected function setObjectManagerForElements(FieldsetInterface $form, $entityManager = null) {
        $entityManager = !is_null($entityManager) ? $entityManager : $this->getEntityManager();
        foreach ($form->getElements() as $element) {
            if (method_exists($element, 'setObjectManager'))
                $element->setObjectManager($entityManager);
            else {
                if (method_exists($element, 'getProxy')) {
                    $proxy = $element->getProxy();
                    if (method_exists($proxy, 'setObjectManager'))
                        $proxy->setObjectManager($entityManager);
                }
            }
        }
        foreach ($form->getFieldsets() as $fieldset) /** @var FieldsetInterface $fieldset */ {
            if (method_exists($fieldset, 'getTargetElement')) {
                $this->setObjectManagerForElements($fieldset->getTargetElement(), $entityManager);
            } else {
                $this->setObjectManagerForElements($fieldset, $entityManager);
            }
        }
    }    
}