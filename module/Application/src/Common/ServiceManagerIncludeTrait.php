<?php

namespace Application\Common;


/**
 * Trait ServiceManagerIncludeTrait
 *
 * @package Application\Common
 */
trait ServiceManagerIncludeTrait {
    
    /**
     * @var
     */
    protected $serviceManager;
    
    /**
     * @return mixed
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }
    
    /**
     * @param mixed $serviceManager
     */
    public function setServiceManager($serviceManager) {
        $this->serviceManager = $serviceManager;
    }
    
}