<?php
namespace Application\Common;

use Zend\Form\Fieldset;
/**
 * Class CsrfAndSubmitFieldsetTrait
 *
 * @package Application\Common
 */
trait CsrfAndSubmitFieldsetTrait {
    
    /**
     * @param Fieldset $formOrFieldset
     */
    public function addCsrfAndSubmitButton(Fieldset $formOrFieldset) {
    
        $formOrFieldset->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);
    
        $formOrFieldset->add([
            'type'  => 'button',
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'class' => 'button expanded',
            ],
            'options' => [
                'label' => 'Submit',
                'label_options' => [
                    'class' => 'text-right middle'
                ]
            ],
        ]);
    }
}