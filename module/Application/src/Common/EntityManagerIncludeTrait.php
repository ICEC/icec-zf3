<?php
namespace Application\Common;

/**
 * Trait EntityManagerIncludeTrait
 *
 * @package Application\Common
 */
trait EntityManagerIncludeTrait {
    /**
     * @var
     */
    protected $entityManager;
    
    /**
     * @return mixed
     */
    public function getEntityManager() {
        return $this->entityManager;
    }
    
    /**
     * @param mixed $entityManager
     */
    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
    }
}