<?
namespace Application\Common;

/**
 * Trait YearWeeksAsOptionsTrait
 *
 * @package module\Application\src\Common
 */
trait YearWeeksAsOptionsTrait {
    
    /**
     * @return array
     */
    public static function weeks() {
        $weeks = [];
        $nextWeek = strtotime('+1 week');
        for ($i = 0; $i < date('W'); $i++) {
            $date = date('Y-m-d', strtotime('-' . $i . ' week'));
            $nbDay = date('N', strtotime($date));
            
            $monday = new \DateTime($date);
            $sunday = new \DateTime($date);
            
            $monday->modify('-' . ($nbDay - 1) . ' days');
            $sunday->modify('+' . (7 - $nbDay) . ' days');
            
            if ($nextWeek > strtotime($sunday->format('Y-m-d'))) {
                $weeks[$monday->format('W')] = [
                    'value' => $monday->format('W'),
                    'label' => 'Week # ' . $monday->format('W') . ' | ' . $monday->format('j M Y') . ' - ' . $sunday->format('j M Y'),
                    'attributes' => [
                        'data-start' => $monday->format('Y-m-d'),
                        'data-end' => $sunday->format('Y-m-d'),
                    ],
                
                ];
            }
        }
        return $weeks;
    }
}