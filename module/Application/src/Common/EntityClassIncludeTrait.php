<?php
namespace Application\Common;

/**
 * Trait EntityClassIncludeTrait
 *
 * @package module\Application\src\Common
 */
trait EntityClassIncludeTrait {
    
    /**
     * @var
     */
    protected $entityClass;
    
    /**
     * @return mixed
     */
    public function getEntityClass() {
        return $this->entityClass;
    }
    
    /**
     * @param mixed $entityClass
     */
    public function setEntityClass($entityClass) {
        $this->entityClass = $entityClass;
    }
}