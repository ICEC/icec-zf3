<?php

namespace Application\Common;

use Zend\Log;

trait LoggerTrait {
    
    /**
     *
     * Creates a Logger to be used during the Import process
     *
     * @param string $logFile The location of the log file, relative to the Zend App root
     * @return \Zend\Log\Logger
     */
    private function getLogger($logFile) {
        $logger = new Log\Logger;
        $logger->addWriter(new Log\Writer\Stream($logFile));
        $logger->addWriter((new Log\Writer\Stream('php://output'))->addFilter(new Log\Filter\Priority(Log\Logger::NOTICE)));
        return $logger;
    }
}