<?php
namespace Application\Common;

use Zend\Form\Fieldset;
/**
 * Class CsrfFieldsetTrait
 *
 * @package Application\Common
 */
trait CsrfFieldsetTrait {
    
    /**
     * @param Fieldset $formOrFieldset
     */
    public function addCsrf(Fieldset $formOrFieldset) {    
        $formOrFieldset->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'attributes' => [
                'id' => 'csrf',
            ],
        ]); 
    }
}