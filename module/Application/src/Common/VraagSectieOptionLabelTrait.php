<?

namespace Application\Common;

/**
 * Trait VraagSectieOptionLabelTrait
 *
 * @package module\Application\src\Common
 */
trait VraagSectieOptionLabelTrait {
    
    /**
     * @param $sectie_id
     * @return mixed
     */
    public static function getVraagSectieLabel($sectie_id) {
        $sectieOpties = [
            '4' => 'Getallen Vragen enkel',
            '1' => 'Getallen Vragen dubbel',
            '2' => 'Checks',
            '3' => 'Uitgevoerd Vragen',
            '5' => 'Afsluiting Vragen',
        ];
        return $sectieOpties[$sectie_id];
    }
}