<?

namespace Application\Common;

/**
 * Trait ArtikelEenheidTrait
 *
 * @package module\Application\src\Common
 */
trait ArtikelEenheidTrait {
    
    /**
     * @param $eenheid_id
     * @return string
     */
    public static function printArtikelEenheid($eenheid_id) {
        switch ($eenheid_id) {
            case 0:
                $eenheid = 'stuk';
                break;
            case 1:
                $eenheid = 'kg';
                break;
            case 2:
                $eenheid = 'meter';
                break;
            case 3:
                $eenheid = 'liter';
                break;
            case 4:
                $eenheid = 'cm';
                break;
            default:
                $eenheid = 'stuk';
        }
        return $eenheid;
    }
}