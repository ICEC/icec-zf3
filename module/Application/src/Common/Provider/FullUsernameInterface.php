<?php
namespace Application\Common\Provider;
/**
 * Interface FullUsernameInterface
 *
 *      Ensures we have a users full name
 * @package Application\Common\Provider
 */
interface FullUsernameInterface {
    
    /**
     * @return mixed
     */
    public function getVoornaam();
    
    /**
     * @return mixed
     */
    public function getAchternaam();
}