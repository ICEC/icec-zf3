<?php
namespace Application\Service;

use Application\Entity\User;
/**
 * Class AuthService
 *
 * @package Application\Service
 */
class AuthService extends AbstractEntityService{
    
    /**
     *
     */
    const ENTITY_CLASS = User::class;
}