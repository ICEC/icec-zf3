<?php

namespace Application\Service;

use CirclicalUser\Mapper\GroupPermissionMapper;
use CirclicalUser\Provider\RoleInterface;
use CirclicalUser\Provider\UserInterface;
use Doctrine\ORM\EntityRepository;

class GroupPermission {
    const ACTIONS_KEY = "actions";
    
    /** @var array */
    protected $guardRules;
    /** @var GroupPermissionMapper */
    protected $permissionProvider;
    
    /** @var array */
    protected $sortedGuardRules;
    /** @var array */
    protected $roleGuardRules;
    
    /**
     * GroupPermission constructor.
     *
     * @param array $rules
     * @param GroupPermissionMapper $permissionProvider
     */
    public function __construct(
        array $rules,
        GroupPermissionMapper $permissionProvider
    ) {
        $this->guardRules = $rules;
        $this->permissionProvider = $permissionProvider;
    }
    
    /**
     * @param RoleInterface $role
     * @param array $allowed
     * @param array $denied
     */
    public function persistPermissionChanges(RoleInterface $role, array $allowed, array $denied) {
        try {
            (new Helper\GroupPermission())(
                $this->permissionProvider,
                $role,
                $this->getPermissionsByRole($role),
                $allowed,
                $denied
            );
        } catch (\Exception $e) {
            // TODO: handle exception
            $var = 1;
        }
    }
    
    /**
     * @param UserInterface $user
     * @param bool $useRoleParents
     * @return array
     */
    public function getPermissionsByUserRoles(UserInterface $user, bool $useRoleParents = false) {
        $repository = $this->getRepository();
        
        $roles = $user->getRoles();
        $permissions = [];
        foreach ($roles as $role) {
            $found = $repository->findBy(['role' => $role]);
            if ($useRoleParents) {
                $found = array_merge($found, $this->getPermissionsByRoleParent($role, $useRoleParents));
            }
            $permissions = array_merge($permissions, $found);
        }
        return $permissions;
    }
    
    /**
     * @param RoleInterface $role
     * @return \CirclicalUser\Entity\GroupPermission[]
     */
    public function getPermissionsByRole(RoleInterface $role): array {
        $repository = $this->getRepository();
        $found = $repository->findBy(['role' => $role]);
        return $found;
    }
    
    /**
     * @param RoleInterface $role
     * @param bool $useRoleParents
     * @return array
     */
    public function getPermissionsByRoleParent(RoleInterface $role, bool $useRoleParents = false): array {
        $repository = $this->getRepository();
        
        $parent = $role->getParent();
        $permissions = [];
        if ($parent !== null) {
            $permissions = $repository->findBy(['role' => $parent]);
            $permissions = array_merge($permissions, $this->getRoleGuardRules($parent));
            $permissions = array_merge($permissions, $useRoleParents ? $this->getPermissionsByRoleParent($parent, $useRoleParents) : []);
        }
        return $permissions;
    }
    
    /**
     * Takes the guard rules from the config and sorts them to make them more easily usable
     */
    public function sortGuardsRules() {
        $this->sortedGuardRules = [];
        $this->roleGuardRules = [];
        foreach ($this->guardRules as $module) {
            foreach ($module as $moduleControllers) {
                foreach ($moduleControllers as $controllerName => $controller) {
                    foreach ($controller as $action => $roles) {
                        if ($action === static::ACTIONS_KEY) {
                            foreach ($roles as $nestedAction => $nestedRoles) {
                                $this->createRoleGuardRules($controllerName, $nestedAction, $nestedRoles);
                            }
                            continue;
                        }
    
                        $this->createRoleGuardRules($controllerName, $action, $roles);
                    }
                }
            }
        }
    }
    
    /**
     * @param RoleInterface|null $roleToExclude
     * @param bool $excludeAllGranted
     * @return array
     */
    public function getSortedGuardRules(RoleInterface $roleToExclude = null, bool $excludeAllGranted = true): array {
        if (!isset($this->sortedGuardRules)) {
            $this->sortGuardsRules();
        }
        
        $return = $this->sortedGuardRules;
        if ($roleToExclude !== null) {
            foreach ($return as $key => &$rule) {
                if (($excludeAllGranted && empty($rule['roles'])) || $this->recursiveRoleNameInArray($roleToExclude, $rule['roles'])) {
                    unset($return[$key]);
                }
            }
        }
        
        return $return;
    }
    
    protected function recursiveRoleNameInArray(RoleInterface $role, array $array): bool {
        /** @var RoleInterface $parent */
        $parent = $role->getParent();
        if ($parent !== null && !$parent instanceof RoleInterface) {
            throw new \Exception("Handle this");
        }
        return in_array($role->getName(), $array) || ($parent !== null && $this->recursiveRoleNameInArray($parent, $array));
    }
    
    /**
     * @return array
     */
    public function getRoleGuardRules(RoleInterface $role): array {
        if (!isset($this->roleGuardRules)) {
            $this->sortGuardsRules();
        }
        return isset($this->roleGuardRules[$role->getName()]) ? $this->roleGuardRules[$role->getName()] : [];
    }
    
    /**
     * @return EntityRepository
     */
    protected function getRepository(): EntityRepository {
        return $this->permissionProvider->getRepository();
    }
    
    /**
     * @param $controllerName
     * @param $action
     * @param $roles
     */
    protected function createRoleGuardRules($controllerName, $action, $roles): void {
        $fullName = $controllerName . '::' . $action;
        $this->sortedGuardRules[$fullName] = [
            'controller' => $controllerName,
            'action' => $action,
            'roles' => $roles,
        ];
        
        foreach ($roles as $role) {
            if (!isset($this->roleGuardRules[$role])) {
                $this->roleGuardRules[$role] = [];
            }
            $this->roleGuardRules[$role][$fullName] = [
                'controller' => $controllerName,
                'action' => $action,
            ];
        }
    }
    
}