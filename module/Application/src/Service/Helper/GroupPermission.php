<?php

namespace Application\Service\Helper;

use CirclicalUser\Mapper\GroupPermissionMapper;
use CirclicalUser\Provider\RoleInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GroupPermission
{
    /** @var GroupPermissionMapper */
    protected $permissionProvider;
    /** @var ObjectManager */
    protected $em;

    /** @var RoleInterface */
    protected $role;
    /** @var \CirclicalUser\Entity\GroupPermission[] */
    protected $currentPermissions;

    /**
     * @param GroupPermissionMapper $permissionProvider
     * @param RoleInterface $role
     * @param array $currentPermissions
     * @param array $allowed
     * @param array $denied
     */
    public function __invoke(GroupPermissionMapper $permissionProvider, RoleInterface $role, array $currentPermissions, array $allowed, array $denied)
    {
        $this->permissionProvider = $permissionProvider;
        $this->em = $permissionProvider->getEntityManager();
        $this->role = $role;
        $this->setCurrentPermissions($currentPermissions);

        $this->createAllowedPermissions($allowed);
        $this->deleteDeniedPermissions($denied);
        $this->em->flush();
    }

    /**
     * @param array $permissions
     * @throws \Exception
     */
    protected function deleteDeniedPermissions(array $permissions)
    {
        foreach ($permissions as $permission) {
            if (!isset($this->currentPermissions[$permission])) {
                continue; // skip if denied permission was already denied
            }

            $permissionEntity = $this->currentPermissions[$permission];

            // Check if entity should be removed or just altered
            if (count($permissionEntity->getActions()) > 1) {
                list($controllerName, $actionName) = explode('::', $permission);
                // TODO: throw exception when $permission doesn't have 2 elements OR handle that
                if ($controllerName === null || $actionName === null) {
                    throw new \Exception("Expected explode to return 2 elements");
                }

                $permissionEntity->removeAction($actionName);
                $this->em->persist($permissionEntity);
                continue;
            }
            $this->em->remove($permissionEntity);
        }
    }

    /**
     * @param array $permissions
     * @throws \Exception
     */
    protected function createAllowedPermissions(array $permissions)
    {
        foreach ($permissions as $permission) {
            if (isset($this->currentPermissions[$permission])) {
                continue; // skip if allowed permission was already allowed
            }

            list($controllerName, $actionName) = explode('::', $permission);
            // TODO: throw exception when $permission doesn't have 2 elements OR handle that
            if ($controllerName === null || $actionName === null) {
                throw new \Exception("Expected explode to return 2 elements");
            }

            // Check if an entity should be created or just altered
            if (isset($this->currentPermissions[$controllerName])) {
                $this->currentPermissions[$controllerName]->addAction($actionName);
                $this->em->persist($this->currentPermissions[$controllerName]);
                continue;
            }

            $this->currentPermissions[$controllerName] = $permissionEntity = $this->permissionProvider->create(
                $this->role,
                'guard',
                $controllerName,
                [$actionName]
            );
            $this->em->persist($permissionEntity);
        }
    }

    /**
     * @param array $currentPermissions
     */
    protected function setCurrentPermissions(array $currentPermissions)
    {
        foreach ($currentPermissions as $groupPermission) {
            $currentPermissions[$groupPermission->getResourceId()] = $groupPermission;
            foreach ($groupPermission->getActions() as $action) {
                $currentPermissions[$groupPermission->getResourceId() . '::' . $action] = $groupPermission;
            }
        }
        $this->currentPermissions = $currentPermissions;
    }
}