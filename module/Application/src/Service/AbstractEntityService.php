<?php

namespace Application\Service;

use Application\Common\AuthenticationServiceIncludeTrait;
use Application\Common\EntityManagerIncludeTrait;
use Application\Common\FormElementManagerIncludeTrait;
use Application\Entity\AbstractEntity;
use Application\Repository\BaseEntityRepository;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManagerInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\Form\FormInterface;

/**
 * {@inheritDoc}
 */
abstract class AbstractEntityService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;
    use EntityManagerIncludeTrait;
    use FormElementManagerIncludeTrait;
    use AuthenticationServiceIncludeTrait;
    const ENTITY_CLASS = null;
    
    public function __construct(
        EntityManagerInterface $entityManager,
        AuthenticationService $authenticationService,
        FormElementManagerV3Polyfill $FormElementManager
    ) {
        if (!class_exists($this::ENTITY_CLASS)) {
            throw new \Exception('Given entityClass "' . $this::ENTITY_CLASS . '" must exist');
        }

        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->FormElementManager = $FormElementManager;
    }

    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     * @param int|null $lockMode One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        return $this->getRepository()->find($id, $lockMode, $lockVersion);
    }

    /**
     * Finds all entities in the repository.
     *
     * @return array The entities.
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }
    
    
    /**
     * @param FormInterface $form
     * @return AbstractEntity|bool
     * @throws \Exception
     */
    public function persistForm(FormInterface $form) {
        try {
            // TODO: trigger event
            if (!$form->isValid()) {
                return false;
            }
            /** @var AbstractEntity $entity */
            $entity = $form->getData();
            $em = $this->getEntityManager();
            if ($entity->getId() > 0) {
                if (\method_exists($entity, "setUpdatedAt")) {
                    $entity->setUpdatedAt(new \DateTime('now'));
                }
            }else{
                if (\method_exists($entity, "setUpdatedAt")) {
                    $entity->setUpdatedAt(new \DateTime('now'));
                }
                if (\method_exists($entity, "setCreated")) {
                    $entity->setCreated(new \DateTime('now'));
                }
            }
            $em->persist($entity);
            $em->flush();
        } catch (\Exception $e) {
            // TODO: trigger event
            throw $e;
        }
        return $entity;
    }
    
    /**
     * @param array $messages
     * @return string
     */
    protected function stringifyMessages(array $messages) {
        return implode($this->flattenArrayRecursive($messages), ", ");
    }

    protected function flattenArrayRecursive(array $array, $path = "", $flattenedArray = []) {
        foreach ($array as $k => $v) {
            if (!is_array($v)) {
                $flattenedArray[] = $path . ": " .$v;
            } else {
                $flattenedArray = $this->flattenArrayRecursive($v, $k, $flattenedArray);
            }
        }
        return $flattenedArray;
    }

    public function delete($id) {
        try {
            // TODO: trigger event
            if (is_null($entity = $this->getRepository()->find($id))) {
                throw new \Exception('Entity with given id does not exist');
            }

            $em = $this->getEntityManager();

            if ($entity->getId() > 0) {
                $entity->setDeletedAt(new \DateTime());// TODO: should be done by a listener
            }

            $em->persist($entity);
            $em->flush();
        } catch (\Exception $e) {
            // TODO: trigger event
            throw $e;
        }
        return $entity;
    }
    // region helpers
    
    /**
     * @return BaseEntityRepository
     */
    protected function getRepository()
    {
        return $this->getEntityManager()->getRepository($this::ENTITY_CLASS);
    }
    // endregion


    public function save($form, $entity)
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.init', $this, [
            'entity' => $entity,
            'form' => $form,
        ]);
        if (!$form->isValid()) {
            $this->getEventManager()->trigger(__FUNCTION__ . '.invalid', $this, [
                'entity' => $entity,
                'form' => $form,
            ]);
            //            return $form->getMessages(); // TODO: is this required for AJAX call feedback?
            return false;
        }
        $em = $this->getEntityManager();
        $entity = $form->getData();
        if ($entity->getId() > 0) {
            $entity = $em->merge($entity);
            if (\method_exists($entity, "setUpdatedAt")) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
        }else{
            if (\method_exists($entity, "setUpdatedAt")) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
            if (\method_exists($entity, "setCreated")) {
                $entity->setCreated(new \DateTime('now'));
            }
        }
        $this->getEventManager()->trigger(__FUNCTION__, $this, [
            'entity' => $entity,
            'form' => $form,
        ]);
        $em->persist($entity);
        $em->flush();
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this, [
            'entity' => $entity,
            'form' => $form,
        ]);

        return $entity;
    }

}