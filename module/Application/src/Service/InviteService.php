<?php
namespace Application\Service;

use Application\Entity\User;
use CirclicalUser\Entity\Authentication;
use CirclicalUser\Exception\TooManyRecoveryAttemptsException;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;

class InviteService {
    
    protected $entityManager;
    protected $authenticationService;
    protected $mailService;


    public function __construct(EntityManager $entityManager,AuthenticationService $authenticationService,MailService $mailService) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->mailService = $mailService;
    }

    /**
     * @return mixed
     */
    public function getAuthenticationService() {
        return $this->authenticationService;
    }

    /**
     * @return MailService
     */
    public function getMailService() {
        return $this->mailService;
    }




    public function getEntityManager() {
        return $this->entityManager;
    }

    public function InviteUserByMail($user_id) {
        /** @var \Application\Entity\User $userEntity */
        $userEntity = $this->getEntityManager()->getRepository(User::class)->findOneBy(['id' => $user_id]);
        if(!is_object($userEntity)){
            return 'User not found';
        }
        $this->ResetPassword($userEntity);
        try {
            $resetToken = $this->getAuthenticationService()->createRecoveryToken($userEntity);
    
            $this->getMailService()->InviteUserToResetPassword($userEntity,$resetToken);

            
        } catch (\Throwable $ex) {
            if ($ex instanceof TooManyRecoveryAttemptsException) {
                return 'Too Many Recovery Attempts';
            }
        }
        return true;
    }


    /**
     * If Auth user not existing create one first,
     *              then set/reset password to a random string
     *
     * @param User $user
     * @throws \CirclicalUser\Exception\NoSuchUserException
     * @throws \CirclicalUser\Exception\EmailUsernameTakenException
     * @throws \CirclicalUser\Exception\MismatchedEmailsException
     * @throws \CirclicalUser\Exception\PersistedUserRequiredException
     * @throws \CirclicalUser\Exception\UsernameTakenException
     */
    private function ResetPassword(User $user) {
        if(!is_object($this->getEntityManager()->getRepository(Authentication::class)->findOneBy(['user_id' => $user->getId()]))){
            $this->getAuthenticationService()->registerAuthenticationRecord($user,$user->getEmail(),'ihKP163#L0vAW#BxF7n8Jo6M');
        }else{
            $this->getAuthenticationService()->resetPassword($user,'ihKP163#L0vAW#BxF7n8Jo6M');
        }
    }
}