<?php


namespace Application\Service\Factory;

use Application\Controller\Factory\AbstractControllerFactory;
use Application\Service\AbstractEntityService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class UserAuthServiceFactory extends AbstractControllerFactory
{
    /** @var \ReflectionClass */
    private $reflectionClass;

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return AbstractEntityService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(EntityManager::class),
            $container->get(AuthenticationService::class),
            $container->get('FormElementManager')
        );
    }

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        if (!class_exists($requestedName)) {
            return false;
        }
        $this->reflectionClass = new \ReflectionClass($requestedName);
        return $this->reflectionClass->isSubclassOf(AbstractEntityService::class);
    }

}