<?php

namespace Application\Service\Authentication;

use Application\Entity\User;
use Application\Form\AuthForm;
use Application\Service\AbstractEntityService;
use Application\Service\MailService;
use CirclicalUser\Entity\Authentication;
use CirclicalUser\Entity\UserResetToken;
use CirclicalUser\Exception\InvalidResetTokenException;
use CirclicalUser\Exception\NoSuchUserException;
use CirclicalUser\Exception\PasswordResetProhibitedException;
use CirclicalUser\Exception\TooManyRecoveryAttemptsException;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManagerInterface;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;


/**
 * Class AuthenticationService
 *
 * @package Application\Service\Authentication
 */
class UserAuthService extends AbstractEntityService {
    
    /**
     * UserAuthService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param AuthenticationService $authenticationService
     * @param FormElementManagerV3Polyfill $FormElementManager
     * @param MailService $mailService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AuthenticationService $authenticationService,
        FormElementManagerV3Polyfill $FormElementManager,
        MailService $mailService
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->FormElementManager = $FormElementManager;
        $this->mailService = $mailService;
        parent::__construct($entityManager, $authenticationService, $FormElementManager);
    }
    
    /**
     * @var MailService
     */
    protected $mailService;
    
    /**
     *
     */
    const ENTITY_CLASS = User::class;
    
    /**
     * @param AuthForm $form
     * @return bool
     */
    public function TryAuthenticating(AuthForm $form) {
        $data = $form->getData();
        $user = null;
        try {
            $user = $this->getAuthenticationService()->authenticate($data['email'], $data['password']);
        } catch (\Throwable $ex) {
            return false;
        }
        if (is_object($user)) {
            // No errors and we got an User object ,So Authentication succesfull
            return true;
        }
        return false;
    }
    
    /**
     * @param $userEntity
     * @return bool|UserResetToken|string
     */
    public function doPasswordReset($userEntity) {
        $resetToken = $this->ResetUserPassword($userEntity);
        if (!$resetToken instanceof UserResetToken):
            return $resetToken;
        else:
            return $this->SendPassResetMail($userEntity, $resetToken);
        endif;
    }
    
    /**
     * @param User $userEntity
     * @return bool|UserResetToken|string
     */
    protected function ResetUserPassword(User $userEntity) {
        /** @var \Application\Entity\User $userEntity */
        /** @var \CirclicalUser\Service\AuthenticationService $authenticationService */
        $this->ResetPassword($userEntity);
        try {
            $authenticationService = $this->getAuthenticationService();
            $resetToken = $authenticationService->createRecoveryToken($userEntity);
            return $resetToken;
        } catch (\Throwable $ex) {
            if ($ex instanceof TooManyRecoveryAttemptsException) {
                return 'Too Many Recovery Attempts';
            }
        }
        return false;
    }
    
    /**
     * @param $userEntity
     * @param $resetToken
     * @return bool|string
     */
    protected function SendPassResetMail($userEntity, $resetToken) {
        try {
            /** @var MailService $mailService */
            $mailService = $this->getMailService();
            $mailService->InviteUserToResetPassword($userEntity, $resetToken);
        } catch (\Throwable $ex) {
            if ($ex instanceof TooManyRecoveryAttemptsException) {
                return 'Mail Sending error';
            }
        }
        return true;
    }
    
    /**
     * If Auth user not existing create one first,
     *              then set/reset password to a random string
     *
     * @param User $user
     * @throws \CirclicalUser\Exception\NoSuchUserException
     * @throws \CirclicalUser\Exception\EmailUsernameTakenException
     * @throws \CirclicalUser\Exception\MismatchedEmailsException
     * @throws \CirclicalUser\Exception\PersistedUserRequiredException
     * @throws \CirclicalUser\Exception\UsernameTakenException
     */
    private function ResetPassword(User $user) {
        if (!is_object($this->getEntityManager()->getRepository(Authentication::class)->findOneBy(['user_id' => $user->getId()]))) {
            $this->getAuthenticationService()->registerAuthenticationRecord($user, $user->getEmail(), 'ihKP163#L0vAW#BxF7n8Jo6M');
        } else {
            $this->getAuthenticationService()->resetPassword($user, 'ihKP163#L0vAW#BxF7n8Jo6M');
        }
    }
    
    /**
     * @return mixed
     */
    public function getMailService() {
        return $this->mailService;
    }
    
    
    /**
     * @param UserResetToken $resettoken
     * @param $password
     * @return bool|string
     */
    public function doPasswordSet(UserResetToken $resettoken, $password) {
        /** @var User $userEntity */
        $userEntity = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => $resettoken->getAuthentication()->getUsername()]);
        try {
            $this->getAuthenticationService()->changePasswordWithRecoveryToken($userEntity, $resettoken->getId(), $resettoken->getToken(),
                $password);
        } catch (\Throwable $ex) {
            if ($ex instanceof PasswordResetProhibitedException) {
                return 'Password Reset Prohibited Exception';
            }
            if ($ex instanceof NoSuchUserException) {
                return 'No Such User Exception';
            }
            if ($ex instanceof InvalidResetTokenException) {
                return 'Invalid Reset Token Exception';
            }
        }
        return true;
    }
}