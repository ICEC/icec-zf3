<?php

namespace Application\Service;

use Application\Entity\User;
use CirclicalUser\Entity\UserResetToken;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Mail\Protocol\Smtp\Auth\Login;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class MailService
 *
 * @package Application\Service
 */
class MailService {
    
    #region Protected Vars
    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;
    
    /**
     * @var
     */
    protected $smtpConfig;
    /**
     * @var PhpRenderer
     */
    protected $viewRender;
    
    /**
     * @var
     */
    protected $Mailbody;
    
    /**
     * @var string
     */
    protected $defaultSendersEmail = 'noreply@2coolbv.com';
    /**
     * @var string
     */
    protected $defaultSendersName = '2COOLBV';
    /**
     * @var string
     */
    protected $defaultSubject = 'A2C - Wachtwoord gebruikersaccount instellen';
    /**
     * @var string
     */
    protected $defaultReplyToEmail = 'info@2coolbv.com';
    
    protected $mail_partial_path;
    protected $mailViewModelVars = [];
    #endregion
    
    /**
     * MailService constructor.
     *
     * @param EntityManager $entityManager
     * @param AuthenticationService $authenticationService
     * @param PhpRenderer $viewRender
     * @param $smtpConfig
     */
    public function __construct(
        EntityManager $entityManager, AuthenticationService $authenticationService, PhpRenderer $viewRender, $smtpConfig
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->viewRender = $viewRender;
        $this->smtpConfig = $smtpConfig;
    }
    
    //TODO make this class abstract , remove this function from here
    
    /**
     * @param User $medewerker
     * @param UserResetToken $token
     */
    public function InviteUserToResetPassword(User $medewerker, UserResetToken $token) {
        
        $this->setMailViewModelVars([
            'user' => $medewerker,
            'token' => $token,
        ]);
        $this->setMailPartialPath('mailtemplates/authentication/inviteforpasswordset');
        $email_body = $this->getMailHtml();
        $this->configEmailBody($email_body);
        
        $toSendMail = $this->ConfigMail($medewerker->getEmail());
        $this->SendMailWaitForSucces($toSendMail);
    }
    
    protected function SendMailWaitForSucces($toSendMail) {
        $transport = $this->getMailTransport();
        $transport->send($toSendMail);
    
        /** @var Login $connection */
        $connection = $transport->getConnection();
        $response = $connection->getResponse();
        $responseCode = (integer)substr($response[0], 0, 3);
        if ($responseCode == 250):
            $succes = true;
        // TODO mark factuur as sended
        else:
            $succes = false;
        endif;
        return $succes;
    }
    
    /**
     * @return string
     */
    protected function getMailHtml() {
        $viewmodel = new ViewModel($this->getMailViewModelVars());
        $viewmodel->setTemplate($this->getMailPartialPath());
        return $this->getViewRender()->render($viewmodel);
    }
    
    /**
     * @param $toEmail
     * @return Message
     */
    protected function ConfigMail($toEmail) {
        $mail = new Message();
        $mail->setReplyTo($this->getDefaultReplyToEmail());
        $mail->setBody($this->getMailbody());
        $mail->setFrom($this->getDefaultSendersEmail(), $this->getDefaultSendersName());
        $mail->addTo($toEmail);
        $mail->setSubject($this->getDefaultSubject());
        return $mail;
    }
    
    /**
     * @param $email_body
     */
    protected function configEmailBody($email_body) {
        $html = new MimePart($email_body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->setParts(array($html));
        $this->setMailbody($body);
        return ;
    }
    
    #region Getters & Setters
    
    /**
     * @return mixed
     */
    public function getMailbody() {
        return $this->Mailbody;
    }
    
    /**
     * @param mixed $Mailbody
     */
    public function setMailbody($Mailbody) {
        $this->Mailbody = $Mailbody;
    }
    
    /**
     * @return SmtpTransport
     */
    protected function getMailTransport() {
        $transport = new SmtpTransport();
        $transport->setOptions(new SmtpOptions($this->smtpConfig));
        return $transport;
    }
    
    /**
     * @return PhpRenderer
     */
    protected function getViewRender() {
        return $this->viewRender;
    }
    
    /**
     * @return AuthenticationService
     */
    protected function getAuthenticationService() {
        return $this->authenticationService;
    }
    
    /**
     * @return string
     */
    protected function getDefaultReplyToEmail() {
        return $this->defaultReplyToEmail;
    }
    
    /**
     * @return string
     */
    protected function getDefaultSendersEmail() {
        return $this->defaultSendersEmail;
    }
    
    /**
     * @return string
     */
    protected function getDefaultSendersName() {
        return $this->defaultSendersName;
    }
    
    /**
     * @return EntityManager
     */
    protected function getEntityManager() {
        return $this->entityManager;
    }
    
    /**
     * @return mixed
     */
    protected function getSmtpConfig() {
        return $this->smtpConfig;
    }
    
    /**
     * @return string
     */
    public function getDefaultSubject(): string {
        return $this->defaultSubject;
    }
    
    /**
     * @param string $defaultSubject
     */
    public function setDefaultSubject(string $defaultSubject) {
        $this->defaultSubject = $defaultSubject;
    }
    
    /**
     * @return mixed
     */
    public function getMailPartialPath() {
        return $this->mail_partial_path;
    }
    
    /**
     * @param mixed $mail_partial_path
     */
    public function setMailPartialPath($mail_partial_path) {
        $this->mail_partial_path = $mail_partial_path;
    }
    
    
    /**
     * @return array
     */
    public function getMailViewModelVars(): array {
        return $this->mailViewModelVars;
    }
    
    /**
     * @param array $mailViewModelVars
     */
    public function setMailViewModelVars(array $mailViewModelVars) {
        $this->mailViewModelVars = $mailViewModelVars;
    }
    
    #endregion
}