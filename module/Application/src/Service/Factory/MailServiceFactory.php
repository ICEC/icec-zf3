<?php
namespace Application\Service\Factory;

use Application\Service\AbstractEntityService;
use Application\Service\MailService;
use CirclicalUser\Exception\ConfigurationException;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class MailServiceFactory
 *
 * @package Application\Service\Factory
 */
class MailServiceFactory
{
    /** @var \ReflectionClass */
    private $reflectionClass;
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return MailService
     * @throws ConfigurationException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');        
        $smtpConfig = $config['mail_service']['smtp_options'];
        if (!isset($config['mail_service']['smtp_options'])) {
            throw new ConfigurationException("No mail configuration is defined. Did you add mail config to local.php ?");
        }
        
        return new $requestedName(
            $container->get(EntityManager::class),
            $container->get(AuthenticationService::class),
            $container->get(PhpRenderer::class),
            $smtpConfig
        );
    }

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        if (!class_exists($requestedName)) {
            return false;
        }
        $this->reflectionClass = new \ReflectionClass($requestedName);
        return $this->reflectionClass->isSubclassOf(AbstractEntityService::class);
    }
}