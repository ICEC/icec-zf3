<?php

namespace Application\Service\Factory;

use Application\Service\GroupPermission;
use CirclicalUser\Factory\Service\AccessServiceFactory as BaseAccessServiceFactory;
use CirclicalUser\Mapper\GroupPermissionMapper;
use CirclicalUser\Mapper\RoleMapper;
use CirclicalUser\Mapper\UserMapper;
use CirclicalUser\Mapper\UserPermissionMapper;
use CirclicalUser\Provider\RoleInterface;
use CirclicalUser\Provider\UserInterface;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Interop\Container\ContainerInterface;

class AccessServiceFactory extends BaseAccessServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $userConfig = $config['circlical']['user'];

        if (!isset($userConfig['guards'])) {
            throw new \Exception("You don't have any guards set up! Please follow the steps in the readme.  Define an empty guards definition to get rid of this error.");
        }

        $guards = $userConfig['guards'] ?? [];

        $authenticationService = $container->get(AuthenticationService::class);
        /** @var UserInterface $user */
        $user = $authenticationService->getIdentity();

        if ($user) {
            /** @var GroupPermission $groupPermissionService */
            $groupPermissionService = $container->get(GroupPermission::class);
            foreach ($user->getRoles() as $role) {
                $this->addPermissionsToGuards($role, $guards, $groupPermissionService->getPermissionsByRole($role));
            }
        }

        $userProvider = isset($userConfig['providers']['user']) ? $userConfig['providers']['user'] : UserMapper::class;
        $roleProvider = isset($userConfig['providers']['role']) ? $userConfig['providers']['role'] : RoleMapper::class;
        $groupRuleProvider = isset($userConfig['providers']['rule']['group']) ? $userConfig['providers']['rule']['group'] : GroupPermissionMapper::class;
        $userRuleProvider = isset($userConfig['providers']['rule']['user']) ? $userConfig['providers']['rule']['user'] : UserPermissionMapper::class;

        $accessService = new AccessService(
            $guards,
            $container->get($roleProvider),
            $container->get($groupRuleProvider),
            $container->get($userRuleProvider),
            $container->get($userProvider)
        );

        if ($user) {
            $accessService->setUser($user);
        }

        return $accessService;
    }

    /**
     * @param array $guards
     * @param GroupPermission[] $permissions
     */
    protected function addPermissionsToGuards(RoleInterface $role, array &$guards, array $permissions)
    {
        $roleName = $role->getName();
        foreach ($guards as &$module) {
            foreach ($module as &$controllers) {
                foreach ($controllers as $controller => &$actions) {
                    foreach ($permissions as $permission) {
                        if ($permission->getResourceId() !== $controller) {
                            continue;
                        }
                        foreach ($actions as $action => &$roles) {
                            foreach ($permission->getActions() as $permissionAction) {
                                if ($action === $permissionAction && !in_array($roleName, $roles)) {
                                    $roles[] = $roleName;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}