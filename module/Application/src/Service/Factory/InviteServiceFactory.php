<?php
namespace Application\Service\Factory;

use Application\Service\AbstractEntityService;
use Application\Service\InviteService;
use Application\Service\MailService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
/**
 * Class InviteServiceFactory
 *
 * @package Application\Service\Factory
 */
class InviteServiceFactory 
{
    /** @var \ReflectionClass */
    private $reflectionClass;

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return AbstractEntityService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new InviteService(
            $container->get(EntityManager::class),
            $container->get(AuthenticationService::class),
            $container->get(MailService::class)
        );
    }

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        if (!class_exists($requestedName)) {
            return false;
        }
        $this->reflectionClass = new \ReflectionClass($requestedName);
        return $this->reflectionClass->isSubclassOf(AbstractEntityService::class);
    }
}