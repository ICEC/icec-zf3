<?

namespace Application\Service;

use Application\Repository\UserRepository;

/**
 * Class User
 *
 * @package Application\Service
 */
class User extends AbstractEntityService {
    
    /**
     *
     */
    const ENTITY_CLASS = \Application\Entity\User::class;
    
    /**
     * @param $user_id
     * @param $email_input
     * @return array
     */
    public function ValidateInputedEmail($user_id, $email_input) {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getRepository();
        $result = $userRepository->checkIfEmailExists($email_input);
        $error_text = '';
        $email_bestaat = $show_as_valid = false;
        
        if (key_exists(0, $result)): /** @var \Application\Entity\User $user */
            $user = $result[0];
            if (((integer)$user->getId()) == $user_id):
                $show_as_valid = true;
            else:
                $error_text = 'email al bekend binnen A2C';
            endif;
            $email_bestaat = true;
        else:
            $show_as_valid = true;
        endif;
        
        return [
            'email_bestaat' => $email_bestaat,
            'show_as_valid' => $show_as_valid,
            'error_text' => $error_text,
        ];
    }
}
