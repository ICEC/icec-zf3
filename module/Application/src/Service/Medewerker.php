<?php

namespace Application\Service;

use Application\Controller\Plugin\AuthenticationPlugin;
use Application\Entity\User;
use Application\Repository\MedewerkerRepository;
use CirclicalUser\Entity\Authentication;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;

/**
 * Class Medewerker
 *
 * @package Application\Service
 */
class Medewerker extends AbstractEntityService {
    
    
    /**
     *
     */
    const ENTITY_CLASS = Medewerker::class;
    
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;
    
    
    public function __construct(
        EntityManager $entityManager,
        AuthenticationService $authenticationService,
        $formElementManager
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
    }
    
    /**
     * @param $form
     * @param $entity
     * @return bool|object
     */
    public function save($form, $entity) {
        $this->getEventManager()->trigger(__FUNCTION__ . '.init', $this, [
            'entity' => $entity,
            'form' => $form,
        ]);
        if (!$form->isValid()) {
            $this->getEventManager()->trigger(__FUNCTION__ . '.invalid', $this, [
                'entity' => $entity,
                'form' => $form,
            ]);
            //            return $form->getMessages(); // TODO: is this required for AJAX call feedback?
            return false;
        }
        $em = $this->getEntityManager();
        $entity = $form->getData();
        if ($entity->getId() > 0) {
            $entity = $em->merge($entity);
            if (\method_exists($entity, "setUpdatedAt")) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
        } else {
            if (\method_exists($entity, "setUpdatedAt")) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
            if (\method_exists($entity, "setCreated")) {
                $entity->setCreated(new \DateTime('now'));
            }
        }
        $this->getEventManager()->trigger(__FUNCTION__, $this, [
            'entity' => $entity,
            'form' => $form,
        ]);
        $em->persist($entity);
        $em->flush();
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this, [
            'entity' => $entity,
            'form' => $form,
        ]);
        
        //keep auth in sync
        /** @var AuthenticationPlugin $authenticationPlugin */
        /** @var AuthenticationService $authenticationService */
        /** @var User $userEntity */
        $userEntity = $this->getEntityManager()->getRepository(User::class)->findOneBy(['id' => $entity->getId()]);
        $authuserEntity = $this->getEntityManager()
            ->getRepository(Authentication::class)->findOneBy(['username' => $userEntity->getEmail()]);
        $this->updateAuthUsername($userEntity, $authuserEntity);
        return $entity;
    }
    
    /**
     * @param $userEntity
     * @param $authuserEntity
     * @throws \CirclicalUser\Exception\NoSuchUserException
     * @throws \CirclicalUser\Exception\UsernameTakenException
     */
    private function updateAuthUsername($userEntity, $authuserEntity) {
        /** @var User $userEntity */
        if (is_object($authuserEntity)) {
            $this->getAuthenticationService()->changeUsername($userEntity, $userEntity->getEmail());
        }
    }
    
    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService() {
        return $this->authenticationService;
    }
    
    public function getMedewerkersAtLeastFinancieelAsOptions() {
        /** @var EntityManager $entityManager */
        /** @var MedewerkerRepository $medewerkerRepository */
        $entityManager = $this->getEntityManager();
        $medewerkerRepository = $entityManager->getRepository(\Application\Entity\Medewerker::class);
        $medewerkers = $medewerkerRepository->getMedewerkersMedewerkers();
        
        $options = [];
        foreach ($medewerkers as $medewerker):
            if ($this->CheckUserForRole($medewerker)):
                /** @var \Application\Entity\Medewerker $medewerker */
                array_push($options, [
                    'value' => $medewerker->getId(),
                    'label' => $medewerker->getVoornaam() . ' ' . $medewerker->getAchternaam(),
                ]);
            endif;
        endforeach;
        return $options;
    }
    
    private function CheckUserForRole(\Application\Entity\Medewerker $medewerker) {
        foreach ($medewerker->getRoles() as $role):
            /** @var \CirclicalUser\Entity\Role $role */
            $rolename = $role->getName();
            if (strcmp($rolename, 'Financieel') == 0
                or strcmp($rolename, 'Manager') == 0
                or strcmp($rolename, 'Admin') == 0
            ):
                return true;
            endif;
        endforeach;
        return false;
    }
    
}