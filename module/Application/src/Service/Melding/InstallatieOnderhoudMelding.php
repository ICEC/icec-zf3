<?php
namespace Application\Service\Melding;

use Application\Service\AbstractEntityService;

/**
 * Class InstallatieOnderhoudMelding
 *
 * @package Application\Service\Melding
 */
class InstallatieOnderhoudMelding extends AbstractEntityService {
        
    /**
     *
     */
    const ENTITY_CLASS = \Application\Entity\Melding\InstallatieOnderhoudMelding::class;
}