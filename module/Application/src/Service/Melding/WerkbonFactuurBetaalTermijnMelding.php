<?

namespace Application\Service\Melding;

use Application\Repository\Melding\WerkbonFactuurBetaalTermijnMeldingRepository;
use Application\Service\AbstractEntityService;
use Doctrine\ORM\EntityManager;
use ServiceEnOnderhoud\Entity\Financieel\Factuur\BaseBonFactuur;
use ServiceEnOnderhoud\Repository\Financieel\Factuur\BaseBonFactuurRepository;

/**
 * Class InstallatieOnderhoudMelding
 *
 * @package Application\Service\Melding
 */
class WerkbonFactuurBetaalTermijnMelding extends AbstractEntityService {
    
    /**
     *
     */
    const ENTITY_CLASS = \Application\Entity\Melding\WerkbonFactuurBetaalTermijnMelding::class;
    
    # region Console Functions
    
    /**
     *
     */
    public function CheckWerkbonFacturenForOpenstaand() {
        /** @var EntityManager $entityManager */
        /** @var BaseBonFactuurRepository $baseBonFactuurRepository */
        $entityManager = $this->getEntityManager();
        $baseBonFactuurRepository = $entityManager->getRepository(BaseBonFactuur::class);
        
        $queryResult = $baseBonFactuurRepository->getAllBaseBonFactuurWithFactuurTotaal();
        $queryResult = $this->addAlVoldaanBedragToQueryResults($queryResult);
        $queryResult = $this->removeFullyPaidFromQueryResults($queryResult);
        $queryResult = $this->addMeldingCntToQueryResults($queryResult);
        $queryResult = $this->removeWithMeldingFormQueryResults($queryResult);
        $this->checkAndSendNewMeldings($queryResult);
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addAlVoldaanBedragToQueryResults($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var BaseBonFactuurRepository $baseBonFactuurRepository */
        $entityManager = $this->getEntityManager();
        $baseBonFactuurRepository = $entityManager->getRepository(BaseBonFactuur::class);
        
        foreach ($queryResult as $key => $basBonFactuur):
            $queryResult[$key]['totaal_al_voldaan'] = $baseBonFactuurRepository->getBaseBonAlVoldaanBedrag($basBonFactuur['id']);
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addMeldingCntToQueryResults($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var WerkbonFactuurBetaalTermijnMeldingRepository $werkbonFactuurBetaalTermijnMeldingRepository */
        $entityManager = $this->getEntityManager();
        $werkbonFactuurBetaalTermijnMeldingRepository = $entityManager->getRepository(\Application\Entity\Melding\WerkbonFactuurBetaalTermijnMelding::class);
        
        foreach ($queryResult as $key => $basBonFactuur):
            $queryResult[$key]['melding_cnt'] = $werkbonFactuurBetaalTermijnMeldingRepository
                ->getWerkbonFactuurBetaalTermijnMeldingCountOfFactuur($basBonFactuur['id']);
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function removeFullyPaidFromQueryResults($queryResult) {
        foreach ($queryResult as $key => $basBonFactuur):
            if ($basBonFactuur['totaal_al_voldaan'] >= $basBonFactuur['te_betalen_bedrag']):
                unset($queryResult[$key]);
            endif;
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function removeWithMeldingFormQueryResults($queryResult) {
        foreach ($queryResult as $key => $basBonFactuur):
            if ($basBonFactuur['melding_cnt'] > 0):
                unset($queryResult[$key]);
            endif;
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     */
    protected function checkAndSendNewMeldings($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var BaseBonFactuurRepository $baseBonFactuurRepository */
        $entityManager = $this->getEntityManager();
        $baseBonFactuurRepository = $entityManager->getRepository(BaseBonFactuur::class);
        foreach ($queryResult as $key => $basBonFactuur):
            $queryResult[$key]['betalings_termijn'] = $baseBonFactuurRepository
                ->getBaseBonFactuurBetalingsTermijn($basBonFactuur['id']);
            if ($this->CheckIfVerlopen($queryResult[$key])):
                $this->addWerkbonFactuurBetaalTermijnMelding($queryResult[$key]);
            endif;
        endforeach;
    }
    
    /**
     * @param $basBonFactuur
     * @return bool
     */
    protected function CheckIfVerlopen($basBonFactuur) {
        /** @var \DateTime $checkdate */
        $updatedDate = $basBonFactuur['created'];
        $updatedDate->modify('+' . $basBonFactuur['betalings_termijn'] . ' days');
        if (new \DateTime() > $updatedDate):
            return true;
        endif;
        return false;
    }
    
    /**
     * @param $basBonFactuurArray
     */
    protected function addWerkbonFactuurBetaalTermijnMelding($basBonFactuurArray) {
        /** @var BaseBonFactuurRepository $baseBonFactuurRepository */
        /** @var BaseBonFactuur $baseBonFactuurEntity */
        $baseBonFactuurRepository = $this->getEntityManager()->getRepository(BaseBonFactuur::class);
        $baseBonFactuurEntity = $baseBonFactuurRepository->find($basBonFactuurArray['id']);
        $melding = new \Application\Entity\Melding\WerkbonFactuurBetaalTermijnMelding();
        $melding->setBasebonfactuur($baseBonFactuurEntity);
        $melding->setDatum($basBonFactuurArray['created']->modify('+' . $basBonFactuurArray['betalings_termijn'] . ' days'));
        $melding->setMeldingText('Werkbon factuur betaaltermijn verstreken');
        $this->getEntityManager()->persist($melding);
        $this->getEntityManager()->flush();
    }
    #endregion
    
    /**
     * @return array|mixed
     */
    public function getAllWerkbonFactuurBetaalTermijnMeldingen() {
        /** @var WerkbonFactuurBetaalTermijnMeldingRepository $werkbonFactuurBetaalTermijnMeldingRepository */
        $werkbonFactuurBetaalTermijnMeldingRepository = $this->getRepository();
        $queryResult = $werkbonFactuurBetaalTermijnMeldingRepository->getWerkbonFactuurBetaalTermijnMeldingen();
        $queryResult = $this->addAlVoldaanToQueryResults($queryResult);
        return $queryResult;
    }
    
    /**
     * @return array|mixed
     */
    public function getAllWerkbonFactuurBetaalTermijnVerborgenMeldingen() {
        /** @var WerkbonFactuurBetaalTermijnMeldingRepository $werkbonFactuurBetaalTermijnMeldingRepository */
        $werkbonFactuurBetaalTermijnMeldingRepository = $this->getRepository();
        $queryResult = $werkbonFactuurBetaalTermijnMeldingRepository->getWerkbonFactuurBetaalTermijnVerborgenMeldingen();
        $queryResult = $this->addAlVoldaanToQueryResults($queryResult);
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addAlVoldaanToQueryResults($queryResult) {
        /** @var BaseBonFactuurRepository $baseBonFactuurRepository */
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();
        $baseBonFactuurRepository = $entityManager->getRepository(BaseBonFactuur::class);
        foreach ($queryResult as $key => $basBonFactuur):
            $queryResult[$key]['totaal_al_voldaan'] = $baseBonFactuurRepository->getBaseBonAlVoldaanBedrag($basBonFactuur['basebonfactuur_id']);
        endforeach;
        return $queryResult;
    }
}