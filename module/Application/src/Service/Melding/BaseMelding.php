<?php
namespace Application\Service\Melding;

use Application\Service\AbstractEntityService;

/**
 * Class BaseMelding
 *
 * @package Application\Service\Melding
 */
class BaseMelding extends AbstractEntityService {
        
    /**
     *
     */
    const ENTITY_CLASS = \Application\Entity\Melding\BaseMelding::class;
}