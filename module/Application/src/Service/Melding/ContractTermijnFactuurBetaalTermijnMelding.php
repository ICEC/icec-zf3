<?
namespace Application\Service\Melding;

use Application\Repository\Melding\ContractTermijnFactuurBetaalTermijnMeldingRepository;
use Application\Service\AbstractEntityService;
use Doctrine\ORM\EntityManager;
use ServiceEnOnderhoud\Entity\Financieel\Factuur\ContractTermijnFactuur;
use ServiceEnOnderhoud\Repository\Financieel\Factuur\ContractTermijnFactuurRepository;

/**
 * Class ContractTermijnFactuurBetaalTermijnMelding
 *
 * @package Application\Service\Melding
 */
class ContractTermijnFactuurBetaalTermijnMelding extends AbstractEntityService {
    
    /**
     *
     */
    const ENTITY_CLASS = \Application\Entity\Melding\ContractTermijnFactuurBetaalTermijnMelding::class;
    
    # region Console Functions
    
    /**
     *
     */
    public function CheckContractTermijnFacturenForOpenstaand() {
        /** @var EntityManager $entityManager */
        /** @var ContractTermijnFactuurRepository $contractTermijnFactuurRepository */
        $entityManager = $this->getEntityManager();
        $contractTermijnFactuurRepository = $entityManager->getRepository(ContractTermijnFactuur::class);
        
        $queryResult = $contractTermijnFactuurRepository->getAllContractTermijnenFacturenWithTotaalBedrag();
        
        $queryResult = $this->addAlVoldaanBedragToQueryResults($queryResult);
        $queryResult = $this->removeFullyPaidFromQueryResults($queryResult);
        $queryResult = $this->addMeldingCntToQueryResults($queryResult);
        $queryResult = $this->removeWithMeldingFormQueryResults($queryResult);
        $this->checkAndSendNewMeldings($queryResult);
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addAlVoldaanBedragToQueryResults($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var ContractTermijnFactuurRepository $contractTermijnFactuurRepository */
        $entityManager = $this->getEntityManager();
        $contractTermijnFactuurRepository = $entityManager->getRepository(ContractTermijnFactuur::class);
        
        foreach ($queryResult as $key => $contractTermijnFactuur):
            $queryResult[$key]['totaal_al_voldaan'] = $contractTermijnFactuurRepository->getAlVoldaanBedragContractTermijnenFactuur($contractTermijnFactuur['id']);
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function removeFullyPaidFromQueryResults($queryResult) {
        foreach ($queryResult as $key => $contractTermijnFactuur):
            if ($contractTermijnFactuur['totaal_al_voldaan'] >= $contractTermijnFactuur['te_betalen_bedrag']):
                unset($queryResult[$key]);
            endif;
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addMeldingCntToQueryResults($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var ContractTermijnFactuurBetaalTermijnMeldingRepository $contractTermijnFactuurBetaalTermijnMeldingRepository */
        $entityManager = $this->getEntityManager();
        $contractTermijnFactuurBetaalTermijnMeldingRepository = $entityManager
            ->getRepository(\Application\Entity\Melding\ContractTermijnFactuurBetaalTermijnMelding::class);
        
        foreach ($queryResult as $key => $contractTermijnFactuur):
            $queryResult[$key]['melding_cnt'] = $contractTermijnFactuurBetaalTermijnMeldingRepository
                ->getContractTermijnFactuurBetaalTermijnMeldingCountOfFactuur($contractTermijnFactuur['id']);
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function removeWithMeldingFormQueryResults($queryResult) {
        foreach ($queryResult as $key => $basBonFactuur):
            if ($basBonFactuur['melding_cnt'] > 0):
                unset($queryResult[$key]);
            endif;
        endforeach;
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     */
    protected function checkAndSendNewMeldings($queryResult) {
        /** @var EntityManager $entityManager */
        /** @var ContractTermijnFactuurRepository $contractTermijnFactuurRepository */
        $entityManager = $this->getEntityManager();
        $contractTermijnFactuurRepository = $entityManager->getRepository(ContractTermijnFactuur::class);
        
        foreach ($queryResult as $key => $contractTermijnFactuur):
            $queryResult[$key]['betalings_termijn'] = $contractTermijnFactuurRepository
                ->getContractTermijnFactuurBetalingsTermijn($contractTermijnFactuur['id']);
                
            if ($this->CheckIfVerlopen($queryResult[$key])):
                $this->addContractTermijnFactuurBetaalTermijnMelding($queryResult[$key]);
            endif;
        endforeach;
    }
    
    /**
     * @param $contractTermijnFactuur
     * @return bool
     */
    protected function CheckIfVerlopen($contractTermijnFactuur) {
        /** @var \DateTime $checkdate */
        /** @var \DateTime $updatedDate */
        $updatedDate = $contractTermijnFactuur['created'];
        $updatedDate->modify('+' . $contractTermijnFactuur['betalings_termijn'] . ' days');
        if (new \DateTime() > $updatedDate):
            return true;
        endif;
        return false;
    }
    
    /**
     * @param $contractTermijnFactuur
     */
    protected function addContractTermijnFactuurBetaalTermijnMelding($contractTermijnFactuur) {
        /** @var ContractTermijnFactuurRepository $contractTermijnFactuurRepository */
        /** @var ContractTermijnFactuur $contractTermijnFactuurEntity */
        $contractTermijnFactuurRepository = $this->getEntityManager()->getRepository(ContractTermijnFactuur::class);
        $contractTermijnFactuurEntity = $contractTermijnFactuurRepository->find($contractTermijnFactuur['id']);
        
        $melding = new \Application\Entity\Melding\ContractTermijnFactuurBetaalTermijnMelding();
        $melding->setContracttermijnfactuur($contractTermijnFactuurEntity);
        $melding->setDatum($contractTermijnFactuur['created']->modify('+' . $contractTermijnFactuur['betalings_termijn'] . ' days'));
        $melding->setMeldingText('Contract termijnfactuur betaaltermijn verstreken');
        $this->getEntityManager()->persist($melding);
        $this->getEntityManager()->flush();
    }
    #endregion
    
    #region Ajax Controller functions
    /**
     * @return array|mixed
     */
    public function getAllContractTermijnFactuurBetaalTermijnMeldingen() {
        /** @var ContractTermijnFactuurBetaalTermijnMeldingRepository $contractTermijnFactuurBetaalTermijnMeldingRepository */
        $contractTermijnFactuurBetaalTermijnMeldingRepository = $this->getRepository();
        $queryResult = $contractTermijnFactuurBetaalTermijnMeldingRepository->getContractTermijnFactuurBetaalTermijnMeldingen();
        $queryResult = $this->addAlVoldaanToQueryResults($queryResult);
        return $queryResult;
    }
    
    /**
     * @return array|mixed
     */
    public function getAllContractTermijnFactuurBetaalTermijnVerborgenMeldingen() {
        /** @var ContractTermijnFactuurBetaalTermijnMeldingRepository $contractTermijnFactuurBetaalTermijnMeldingRepository */
        $contractTermijnFactuurBetaalTermijnMeldingRepository = $this->getRepository();
        $queryResult = $contractTermijnFactuurBetaalTermijnMeldingRepository->getContractTermijnFactuurBetaalTermijnVerborgenMeldingen();
        $queryResult = $this->addAlVoldaanToQueryResults($queryResult);
        return $queryResult;
    }
    
    /**
     * @param $queryResult
     * @return mixed
     */
    protected function addAlVoldaanToQueryResults($queryResult) {
        /** @var ContractTermijnFactuurRepository $contractTermijnFactuurRepository */
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();
        $contractTermijnFactuurRepository = $entityManager->getRepository(ContractTermijnFactuur::class);
        foreach ($queryResult as $key => $basBonFactuur):
            $queryResult[$key]['totaal_al_voldaan'] = $contractTermijnFactuurRepository->getAlVoldaanBedragContractTermijnenFactuur($basBonFactuur['contract_termijn_factuur_id']);
        endforeach;
        return $queryResult;
    }
    #endregion
}