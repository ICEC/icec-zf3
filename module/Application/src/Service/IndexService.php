<?php
namespace Application\Service;

use Application\Entity\User;
/**
 * Class IndexService
 *
 * @package Application\Service
 */
class IndexService extends AbstractEntityService{
    
    /**
     *
     */
    const ENTITY_CLASS = User::class;
}