<?php
namespace Application\Service;

use Application\Common\Entity\EntityServicesTraits;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Dompdf\Dompdf;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AbstractPDFMailService
 *
 * @package ServiceEnOnderhoud\Service\Financieel\Factuur
 */
class AbstractPDFMailService extends MailService {
    use EntityServicesTraits;
    #region Protected Vars
    /**
     * @var Dompdf
     */
    protected $DOMpdf;
    
    /**
     * @var
     */
    protected $entityService;
    
    /**
     * @var
     */
    protected $entityServiceClass;
    
    /**
     * @var
     */
    protected $pdf_partial_path;
    
    /**
     * @var
     */
    protected $attached_file_name;
    
    /**
     * @var array
     */
    protected $pdfViewModelVars = [];
    #endregion
    
    /**
     * AbstractPDFMailService constructor.
     *
     * @param EntityManager $entityManager
     * @param AuthenticationService $authenticationService
     * @param PhpRenderer $viewRender
     * @param $smtpConfig
     * @param Dompdf $DOMpdf
     */
    public function __construct(
        EntityManager $entityManager,
        AuthenticationService $authenticationService,
        PhpRenderer $viewRender,
        $smtpConfig,
        Dompdf $DOMpdf
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->viewRender = $viewRender;
        $this->smtpConfig = $smtpConfig;
        $this->DOMpdf = $DOMpdf;
        parent::__construct($entityManager, $authenticationService, $viewRender, $smtpConfig);
    }
    
    /**
     * @return string
     */
    protected function RenderPdf() {
        error_reporting(E_ERROR | E_PARSE);
        $viewModel = new ViewModel($this->getPdfViewModelVars());
        $viewModel->setTemplate($this->getPdfPartialPath());
        $pdfHtml = $this->getViewRender()->render($viewModel);
        
        $this->getDompdf()->loadHtml($pdfHtml);
        $this->getDompdf()->render();
        return $this->getDompdf()->output();
    }
    
    /**
     * @param $email_body
     */
    protected function configEmailBody($email_body) {
        $attachment = new MimePart($this->RenderPdf());
        $attachment->type = 'application/pdf';
        $attachment->filename = $this->getAttachedFileName() . '.pdf';
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = Mime::ENCODING_BASE64;
        
        $html = new MimePart($email_body);
        $html->type = "text/html";
        
        $body = new MimeMessage();
        $body->setParts(array($html, $attachment));
        
        $this->setMailbody($body);
        return;
    }
    
    #region Getters & Setter
    /**
     * @return Dompdf
     */
    public function getDOMpdf(): Dompdf {
        return $this->DOMpdf;
    }
    
    
    /**
     * @return mixed
     */
    public function getPdfPartialPath() {
        return $this->pdf_partial_path;
    }
    
    /**
     * @param mixed $pdf_partial_path
     */
    public function setPdfPartialPath($pdf_partial_path) {
        $this->pdf_partial_path = $pdf_partial_path;
    }
    
    /**
     * @return array
     */
    public function getPdfViewModelVars(): array {
        return $this->pdfViewModelVars;
    }
    
    /**
     * @param array $pdfViewModelVars
     */
    public function setPdfViewModelVars(array $pdfViewModelVars) {
        $this->pdfViewModelVars = $pdfViewModelVars;
    }
    
    /**
     * @return mixed
     */
    public function getAttachedFileName() {
        return $this->attached_file_name;
    }
    
    /**
     * @param mixed $attached_file_name
     */
    public function setAttachedFileName($attached_file_name) {
        $this->attached_file_name = $attached_file_name;
    }
    #endregion
}