<?php
namespace Application\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
/**
 * Class BaseEntityRepository
 *
 * @package Application\Repository
 */
class BaseEntityRepository extends EntityRepository {
    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function findAll() {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->eq('deleted_at', null));
        return $this->matching($criteria);
    }
    
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null) {
        $persister = $this->_em->getUnitOfWork()->getEntityPersister($this->_entityName);
        
        $criteria['deleted_at'] = null;
        
        return $persister->loadAll($criteria, $orderBy, $limit, $offset);
    }
}