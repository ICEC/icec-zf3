<?

namespace Application\Repository;

use Application\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 *
 * @package Application\Repository
 */
class UserRepository extends EntityRepository {
    
    /**
     * @param $email_input
     * @return array
     */
    public function checkIfEmailExists($email_input) {
        
        $this->getEntityManager()->getFilters()->disable('soft-deleteable');
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select([
            'u',
        ])
            ->from(User::class, 'u')
            ->where('u.email  = :email_input')
            ->andWhere('u.deleted_at IS NULL')
            ->setParameter('email_input', $email_input)
            ->setMaxResults(1);
        $queryResult = $qb->getQuery()->getResult();
        return $queryResult;
    }
}