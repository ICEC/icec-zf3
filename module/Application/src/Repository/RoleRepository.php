<?php

namespace Application\Repository;

use Application\Form\AbstractForm;
use CirclicalUser\Entity\Role;
use Doctrine\ORM\EntityRepository;

/**
 * Class RoleRepository
 *
 * @package Application\Repository
 */
class RoleRepository extends EntityRepository {
    /**
     * @param AbstractForm $form
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findAllExcludeSelf(AbstractForm $form, array $criteria = [], array $orderBy = null, $limit = null, $offset = null) {
        /** @var Role $entity */
        $entity = $form->getObject();
        
        if ((new \ReflectionClass($entity))->hasMethod('getDeletedAt')) {
            $criteria['deleted_at'] = null;
        }
        
        $alias = 'e';
        $aliasWithSeperator = $alias . '.';
        $qb = $this->createQueryBuilder($alias);
        $qb->select($alias)
            ->setMaxResults($limit === null ? 25 : $limit)
            ->setFirstResult($offset === null ? 0 : $offset);
        
        if ($entity->getId() > 0) {
            $qb->where($qb->expr()->neq($aliasWithSeperator . 'id', $entity->getId()));
        }
        
        foreach ($criteria as $field => $value) {
            $expr = $qb->expr()->eq($aliasWithSeperator . $field, $value);
            if ($value === null) {
                $expr = $qb->expr()->isNull($aliasWithSeperator . $field);
            }
            $qb->andWhere($expr);
        }
        
        if ($orderBy !== null) {
            foreach ($criteria as $field => $order) {
                $qb->addOrderBy($order, $aliasWithSeperator . $field);
            }
        }
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * @return array
     */
    public function getMedewerkerRoles() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select([
            'role',
        ])
            ->from(Role::class, 'role')
            ->where('role.id != 8')
            ->andWhere('role.id != 9')
            ->andWhere('role.id != 10');
        $queryResult = $qb->getQuery()->getResult();
        return $queryResult;
    }
    
}