<?php

namespace Application\View\Helper;

use CirclicalUser\Service\AccessService;
use Zend\View\Helper\AbstractHelper;

/**
 * Class FetchAccessServiceViewHelper
 *
 * @package Application\View\Helper
 */
class FetchAccessServiceViewHelper extends AbstractHelper {
    
    /**
     * @var AccessService
     */
    protected $accessService;
    
    /**
     * FetchAccessServiceViewHelper constructor.
     *
     * @param AccessService $accessService
     */
    public function __construct(AccessService $accessService) {
        $this->accessService = $accessService;
    }
    
    /**
     * @return AccessService
     */
    public function __invoke() {
        return $this->getAccessService();
    }
    
    /**
     * @return AccessService
     */
    public function getAccessService(): AccessService {
        return $this->accessService;
    }
}