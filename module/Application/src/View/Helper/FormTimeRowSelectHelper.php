<?php
namespace Application\View\Helper;

use DateTime;
use IntlDateFormatter;
use Zend\Form\Element\DateTimeSelect as DateTimeSelectElement;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\FormDateTimeSelect;

/**
 * Class FormTimeRowSelectHelper
 *
 * @package Application\View\Helper
 */
class FormTimeRowSelectHelper extends FormDateTimeSelect {
    
    /**
     * @throws Exception\ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct()
    {
        parent::__construct();
        
        // Delaying initialization until we know ext/intl is available
        $this->timeType = IntlDateFormatter::LONG;
        $this->setLocale('nl_NL');
       
    }

    /**
     * Render a date element that is composed of six selects
     *
     * @param  ElementInterface $element
     * @return string
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Zend\Form\Exception\DomainException
     */
    public function render(ElementInterface $element)
    {
        #region Unmodified code
        if (! $element instanceof DateTimeSelectElement) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\DateTimeSelect',
                __METHOD__
            ));
        }
        
        $name = $element->getName();
        if ($name === null || $name === '') {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }
        #endregion
        
        $shouldRenderDelimiters = false;
        
        //Use a customized helper so we can print the datetimeselect wraped in a div
        $selectHelper = $this->view->plugin('FormSelectDateTimePartialHelper');
        
        #region Unmodified code
        $pattern      = $this->parsePattern($shouldRenderDelimiters);
        
        $daysOptions   = $this->getDaysOptions($pattern['day']);
        $monthsOptions = $this->getMonthsOptions($pattern['month']);
        $yearOptions   = $this->getYearsOptions($element->getMinYear(), $element->getMaxYear());
        $hourOptions   = $this->getHoursOptions($pattern['hour']);
        $minuteOptions = $this->getMinutesOptions($pattern['minute']);
        $secondOptions = $this->getSecondsOptions($pattern['second']);
        
        $dayElement    = $element->getDayElement()->setValueOptions($daysOptions);
        $monthElement  = $element->getMonthElement()->setValueOptions($monthsOptions);
        $yearElement   = $element->getYearElement()->setValueOptions($yearOptions);
        $hourElement   = $element->getHourElement()->setValueOptions($hourOptions);
        $minuteElement = $element->getMinuteElement()->setValueOptions($minuteOptions);
        $secondElement = $element->getSecondElement()->setValueOptions($secondOptions);
        
        if ($element->shouldCreateEmptyOption()) {
            $dayElement->setEmptyOption('');
            $yearElement->setEmptyOption('');
            $monthElement->setEmptyOption('');
            $hourElement->setEmptyOption('');
            $minuteElement->setEmptyOption('');
            $secondElement->setEmptyOption('');
        }
        
        $data = [];
        $data[$pattern['day']]    = $selectHelper->render($dayElement);
        $data[$pattern['month']]  = $selectHelper->render($monthElement);
        $data[$pattern['year']]   = $selectHelper->render($yearElement);
        $data[$pattern['hour']]   = $selectHelper->render($hourElement);
        $data[$pattern['minute']] = $selectHelper->render($minuteElement);
        
        if ($element->shouldShowSeconds()) {
            $data[$pattern['second']]  = $selectHelper->render($secondElement);
        } else {
            unset($pattern['second']);
            if ($shouldRenderDelimiters) {
                unset($pattern[4]);
            }
        }
        #endregion
        
        unset($pattern['day']);
        unset($pattern['month']);
        unset($pattern['year']);
    
        #region Unmodified code
        $markup = '';
        foreach ($pattern as $key => $value) {
            // Delimiter
            if (is_numeric($key)) {
                $markup .= $value;
            } else {
                $markup .= $data[$value];
            }
        }
        #endregion
        return trim($markup);
    }
    
    /**
     * Create a key => value options for minutes
     *          set only 15 minutes steps
     * @param  string $pattern Pattern to use for minutes
     * @return array
     */
    protected function getMinutesOptions($pattern)
    {
        $result = [
            '00'=> '00',
            '15'=> '15',
            '30'=> '30',
            '45'=> '45',
        ];
        return $result;
    }
    
}