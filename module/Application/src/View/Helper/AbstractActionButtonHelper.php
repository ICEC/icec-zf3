<?

namespace Application\View\Helper;

use Application\Common\Entity\EntityServicesTraits;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Zend\Form\FormElementManager\FormElementManagerV3Polyfill;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AbstractActionButtonHelper
 *
 * @package Application\View\Helper
 */
abstract class AbstractActionButtonHelper {
    use EntityServicesTraits;
    
    #region Getters & Setters
    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     * @var
     */
    protected $entityService;
    
    /**
     * @var FormElementManagerV3Polyfill
     */
    protected $formElementManager;
    
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;
    
    /**
     * @var AccessService
     */
    protected $accessService;
    
    /**
     * @var
     */
    protected $template_path;
    
    /**
     * @var
     */
    protected $phpRenderer;
    
    /**
     * @return mixed
     */
    public function getPhpRenderer() {
        return $this->phpRenderer;
    }
    
    /**
     * @return AccessService
     */
    public function getAccessService(): AccessService {
        return $this->accessService;
    }
    
    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager {
        return $this->entityManager;
    }
    
    /**
     * @return mixed
     */
    public function getTemplatePath() {
        return $this->template_path;
    }
    
    /**
     * @return FormElementManagerV3Polyfill
     */
    public function getFormElementManager(): FormElementManagerV3Polyfill {
        return $this->formElementManager;
    }
    
    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService(): AuthenticationService {
        return $this->authenticationService;
    }
    #endregion
    
    /**
     * AbstractActionButtonHelper constructor.
     *
     * @param EntityManager $entityManager
     * @param AuthenticationService $authenticationService
     * @param FormElementManagerV3Polyfill $formElementManager
     * @param AccessService $accessService
     */public function __construct(
        EntityManager $entityManager,AuthenticationService $authenticationService,FormElementManagerV3Polyfill $formElementManager,AccessService $accessService
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
        $this->formElementManager = $formElementManager;
        $this->accessService = $accessService;
    }
    
    /**
     * @param PhpRenderer $phpRenderer
     * @param $id
     * @return mixed
     */
    public function __invoke(PhpRenderer $phpRenderer, $id) {
        $this->phpRenderer = $phpRenderer;
        return $this->getActionButtonHtml($id);
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function getActionButtonHtml($id) {
        $viewModel = new ViewModel();
        $viewModel->setTemplate($this->getTemplatePath());
        $viewModel->setVariables([
            'data' => $this->getEntityService()->getActionButtonData($id),
        ]);
        return $this->getPhpRenderer()->render($viewModel);
    }
}
