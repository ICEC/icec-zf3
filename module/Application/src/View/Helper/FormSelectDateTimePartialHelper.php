<?php
namespace Application\View\Helper;

use Zend\Form\Element\Select as SelectElement;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\FormSelect;

/**
 * Class FormSelectDateTimePartialHelper
 *
 * @package Application\View\Helper
 */
class FormSelectDateTimePartialHelper extends FormSelect {
    
    /**
     * Render a form <select> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\InvalidArgumentException
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
        #region Unmodified code
        if (! $element instanceof SelectElement) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\Select',
                __METHOD__
            ));
        }
        
        $name   = $element->getName();
        if (empty($name) && $name !== 0) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }
        
        $options = $element->getValueOptions();
        
        if (($emptyOption = $element->getEmptyOption()) !== null) {
            $options = ['' => $emptyOption] + $options;
        }
        
        $attributes = $element->getAttributes();
        $value      = $this->validateMultiValue($element->getValue(), $attributes);
        
        $attributes['name'] = $name;
        if (array_key_exists('multiple', $attributes) && $attributes['multiple']) {
            $attributes['name'] .= '[]';
        }
        $this->validTagAttributes = $this->validSelectAttributes;
        #endregion
        $rendered = sprintf(
            '<div class="large-4 columns"><select %s>%s</select></div>',
            $this->createAttributesString($attributes),
            $this->renderOptions($options, $value)
        );
        #region Unmodified code
        // Render hidden element
        $useHiddenElement = method_exists($element, 'useHiddenElement')
            && method_exists($element, 'getUnselectedValue')
            && $element->useHiddenElement();
        
        if ($useHiddenElement) {
            $rendered = $this->renderHiddenElement($element) . $rendered;
        }
        #endregion
        return $rendered;
    }

    
}