<?php
namespace Application\View\Helper\Factory;


use Application\View\Helper\FoundationFormLabelHelper;
use Interop\Container\ContainerInterface;

class FoundationFormLabelHelperFactory {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new FoundationFormLabelHelper(
        );
    }   
 
}