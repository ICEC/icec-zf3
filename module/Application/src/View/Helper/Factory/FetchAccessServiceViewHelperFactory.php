<?php
namespace Application\View\Helper\Factory;

use Application\View\Helper\FetchAccessServiceViewHelper;
use CirclicalUser\Service\AccessService;
use Interop\Container\ContainerInterface;

/**
 * Class FetchAccessServiceViewHelperFactory
 *
 * @package Application\View\Helper\Factory
 */
class FetchAccessServiceViewHelperFactory {
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return FetchAccessServiceViewHelper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new FetchAccessServiceViewHelper(
            $container->get(AccessService::class)
        );
    }    
}