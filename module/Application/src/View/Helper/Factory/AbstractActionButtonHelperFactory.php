<?

namespace Application\View\Helper\Factory;

use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Relatie\View\Helper\ContactPersoonActionButtonHelper;
use Zend\ServiceManager\ServiceManager;


/**
 * Class AbstractActionButtonHelperFactory
 *
 * @package Application\View\Helper\Factory
 */
class AbstractActionButtonHelperFactory {
    
    /**
     * @var
     */
    protected $viewHelperClassname;
    
    /**
     * @return mixed
     */
    public function getViewHelperClassname() {
        return $this->viewHelperClassname;
    }
    
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return ContactPersoonActionButtonHelper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
        
        $serviceManager = $container->get(ServiceManager::class);
        $entityManager = $container->get(EntityManager::class);
        $entityManager->getFilters()->enable('soft-deleteable');
        
        $authenticationService = $container->get(AuthenticationService::class);
        $formElementManager = $serviceManager->get('FormElementManager');
        $accessService = $container->get(AccessService::class);
        
        return new $this->viewHelperClassname(
            $entityManager, $authenticationService,$formElementManager,$accessService
        );
    }
}