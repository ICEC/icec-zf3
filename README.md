# A2C Installatie

## Installation using Composer


## Open crontab and add cronjobs to system, Fires console commands  
```bash
sudo crontab -e
```
```bash
00 00 * * *  /usr/bin/php /var/www/2cool/public/index.php melding-installatie &> /dev/null

00 00 * * *  /usr/bin/php /var/www/2cool/public/index.php check-werkbon-facturen &> /dev/null

00 00 * * *  /usr/bin/php /var/www/2cool/public/index.php check-contract-termijn-facturen &> /dev/null

```



## For development only (compile css & js with gulp)
```bash
npm init
npm install -D gulp -g --prefix vendor/node_modules --save-dev
npm install -D gulp-sass -g --prefix vendor/node_modules --save-dev
npm install -D gulp-connect -g --prefix vendor/node_modules --save-dev
npm install -D gulp-concat -g --prefix vendor/node_modules --save-dev
npm install -D gulp-minify -g --prefix vendor/node_modules --save-dev

bower install
```

```bash
sudo pecl install libsodium

```
## Add new module to composer.json
    "autoload": {
        "psr-4": {
            "NewModule\\": "module/NewModule/src/"
        }
    },
    
```bash
composer install -o
composer dump-autoload

```

## Development mode
###### Enable or disable commands
```bash
$ composer development-enable  # enable development mode
$ composer development-disable # disable development mode
$ composer development-status  # whether or not development mode is enabled



```
## Add Some handy shortcuts
###### open /home/{USER}/.bash_profile and add following  

```bash
alias valid="vendor/bin/doctrine-module orm:validate-schema"
alias update="vendor/bin/doctrine-module orm:schema-tool:update --force"
alias sqdump="vendor/bin/doctrine-module orm:schema-tool:update --dump-sql"
alias brutal-update="./brutal-update.sh"
alias refresh="./orm-refresh.sh"
```
###### then run 
```bash
source ~/.bash_profile
```






## Running Unit Tests

To run the supplied skeleton unit tests, you need to do one of the following:

- During initial project creation, select to install the MVC testing support.
- After initial project creation, install [zend-test](https://zendframework.github.io/zend-test/):

  ```bash
  $ composer require --dev zendframework/zend-test
  ```

Once testing support is present, you can run the tests using:

```bash
$ ./vendor/bin/phpunit
